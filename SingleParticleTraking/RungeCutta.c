#include "RungeCutta.h"

void calcRungeKutta(diffEqFunc CalculateInteractions, const int* restrict nPars, double* restrict x0, 
                    const int* restrict numOfSteps, const int* restrict initStep, const double* restrict stepTime, 
                    FILE * restrict outFile, int *restrict fileRecordPeriod, void* restrict funcOptions)
{

    double xNext[*nPars];
    
    double k1[*nPars];
    double k2[*nPars];
    double k3[*nPars];
    double k4[*nPars]; 

    for(int i = *initStep; i < *numOfSteps; ++i)
    {
        int tmpIerr;
        int ierr = 0;
        double currentTime = i*stepTime[0];
        double time2pass = currentTime;

        CalculateInteractions(nPars, x0, k1, &time2pass, funcOptions, &tmpIerr);
        ierr+=tmpIerr;

        for(int j=0; j < *nPars; ++j)
            xNext[j] = x0[j] + (*stepTime)*k1[j]/2.0;

        time2pass += stepTime[0]/2.0;
        CalculateInteractions(nPars, xNext, k2, &time2pass, funcOptions, &tmpIerr);
        ierr+=tmpIerr;

        for(int j=0; j < *nPars; ++j)
            xNext[j] = x0[j] + (*stepTime)*k2[j]/2.0;

        CalculateInteractions(nPars, xNext, k3, &time2pass, funcOptions, &tmpIerr);
        ierr+=tmpIerr;

        for(int j=0; j < *nPars; ++j)
            xNext[j] = x0[j] + (*stepTime)*k3[j];

        time2pass = currentTime + stepTime[0];
        CalculateInteractions(nPars, xNext, k4, &time2pass, funcOptions, &tmpIerr);
        ierr+=tmpIerr;

        for(int j=0; j < *nPars; ++j)
            x0[j] += (*stepTime)*(k1[j] + 2*k2[j] + 2*k3[j] + k4[j])/6;

        if (ierr > 0)
        {
            return; //no reason to further calculate if there is an error, 
                    //which means the particle is outside the field map
        }

        // lines of code po print current process
        if (i % (*fileRecordPeriod) == 0)
        {
            for(int j=0; j < *nPars; ++j)
                fprintf(outFile, "%.15e ", x0[j]);
        
            fprintf(outFile, "\n");
        }
        
    }

}