import numpy as np
import matplotlib.pyplot as plt
import matplotlib

matplotlib.use('Agg') # this line is needed for threads safety in using matplotlib

validationTimeStep = 5e-12 #s
zLim = 8.3e-2*3  #m
yLim = 306e-6*3  #m
# zLim = 0.155  #m
# yLim = 0.007  #m
# zLim = 11.25;  #m
# yLim = 0.03;   #m
zFieldMapSteps = 2000
yFieldMapSteps = 100
numOfTimeSteps = 5001 #coresponds to 25 ns

dz = 2*zLim/(zFieldMapSteps-1)
dy = 2*yLim/(yFieldMapSteps-1)

z = np.linspace(-zLim, zLim, zFieldMapSteps)
y = np.linspace(-yLim, yLim, yFieldMapSteps)

Z,Y = np.meshgrid(z,y)

Ez = np.empty((yFieldMapSteps, zFieldMapSteps))
Ey = np.empty((yFieldMapSteps, zFieldMapSteps))

Bx = np.empty((yFieldMapSteps, zFieldMapSteps))


def div(Rho, Z, Frho, Fz):

    div_rho = 1/(Rho[1:,:] - dy/2) * ( (Rho*Frho)[1:,:] - (Rho*Frho)[:-1,:] ) / dy
    div_z =  (Fz[:,1:] - Fz[:,:-1])/dz

    div_rho = div_rho[:,1:]
    div_z = div_z[1:,:]

    return Rho[1:,1:] - dy/2, Z[1:,1:] - dz/2, div_rho + div_z

def rot(Rho, Z, Fphi):

    rot_rho = -1/(Rho[:,1:] - dy/2) * ( (Rho*Fphi)[:,1:] - (Rho*Fphi)[:,:-1] ) / dz
    rot_z = (Fphi[1:,:] - Fphi[:-1,:])/dy

    # rot_rho = (rot_rho[1:,:] + rot_rho[:-1,:])/2
    # rot_z = (rot_z[:,1:] + rot_z[:,:-1])/2

    rot_rho = rot_rho[1:,:]
    rot_z = rot_z[:,1:]

    return Rho[1:,1:] - dy/2, Z[1:,1:] - dz/2, rot_rho, rot_z


fig = plt.figure(figsize=(15, 10))

def SavePlots(timeStepNum):

    file = open(f"Outputs/ProtonFields/timeStepNum{timeStepNum}.txt")

    for iz in range(zFieldMapSteps):
        for iy in range(yFieldMapSteps):

            line = file.readline()
            line = line.split(",")

            Bx[iy,iz] = -float(line[3])

            Ez[iy,iz] = float(line[8])
            Ey[iy,iz] = float(line[7])

    file.close()

    #######################
    ### Electric field
    #######################

    
    plt.subplot(2,2,1)
    plt.streamplot(Z,Y,Ez,Ey, density=2.4, color = np.sqrt(Ez*Ez+Ey*Ey), cmap="Reds")
    cbar = plt.colorbar()
    cbar.ax.set_ylabel('E [V/m]')
    plt.clim(0, 7e8)
    plt.title("E field. Time = {:.4f} ns".format(timeStepNum*validationTimeStep*1e9))
    plt.xlabel("z [m]")
    plt.ylabel("y [m]")
    plt.xlim([-zLim,zLim])
    plt.ylim([-yLim,yLim])


    #######################
    ### Charge Density
    #######################

    Erho = Ey[int(yFieldMapSteps/2):,:]
    Ezcyl= Ez[int(yFieldMapSteps/2):,:]
    rho  =  Y[int(yFieldMapSteps/2):,:]
    Zcyl =  Z[int(yFieldMapSteps/2):,:]

    rho_d, Zcyl_d, chargeDensity = div(rho, Zcyl, Erho, Ezcyl)

    eps0 = 8.8541878128e-12
    chargeDensity = chargeDensity * eps0

    ax = plt.subplot(2,2,2)
    plt.contourf(Zcyl_d, rho_d, chargeDensity, 20) 
    cbar = plt.colorbar()
    cbar.ax.set_ylabel('$\\rho$ [C/m$^3$]')
    plt.clim(None, 0.28)
    plt.xlabel("z [m]")
    plt.ylabel("r [m]")
    plt.title("Charge Density")

    # ##########################
    # ### Charge Density sigmas
    # ##########################

    # Zcyl4sigma = Zcyl[1:, 1:] - dz/2
    # rho4sigma = rho[1:, 1:] - dy/2

    # sigma_z = np.sqrt(np.mean(Zcyl4sigma**2 * chargeDensity)/np.mean(chargeDensity))
    # sigma_rho = np.sqrt(np.mean(rho4sigma**2 * chargeDensity)/np.mean(chargeDensity))
    # Q = np.sum(chargeDensity*rho4sigma)*dz*dy*2*np.pi

    # plt.text(0.05, 0.9, "$\\sigma_z = {:.4e}$ m".format(sigma_z), transform=ax.transAxes, c="w")
    # plt.text(0.05, 0.83, "$\\sigma_r = {:.4e}$ m".format(sigma_rho), transform=ax.transAxes, c="w")
    # plt.text(0.05, 0.76, "$Q = {:.4e}$ C".format(Q), transform=ax.transAxes, c="w")


    ##########################
    ### Magnetic field
    ##########################

    ax = plt.subplot(2,2,3)
    plt.contourf(Z, Y, np.abs(Bx), 20) 
    cbar = plt.colorbar()
    cbar.ax.set_ylabel('$B_{\\varphi}$ [T]')
    plt.clim(None, 0.009)
    plt.xlabel("z [m]")
    plt.ylabel("y [m]")
    plt.title("Magnetic field")

    
    ##########################
    ### Current Density
    ##########################

    file_next = open(f"Outputs/ProtonFields/timeStepNum{timeStepNum+1}.txt")

    Ez_next = np.empty((yFieldMapSteps, zFieldMapSteps))
    Ey_next = np.empty((yFieldMapSteps, zFieldMapSteps))

    for iz in range(zFieldMapSteps):
        for iy in range(yFieldMapSteps):

            line = file_next.readline()
            line = line.split(",")

            Ez_next[iy,iz] = float(line[8])
            Ey_next[iy,iz] = float(line[7])
    file_next.close()

    dEzdt = (Ez_next - Ez) / validationTimeStep
    dEydt = (Ey_next - Ey) / validationTimeStep


    rho  =  Y[int(yFieldMapSteps/2):,:]
    Zcyl =  Z[int(yFieldMapSteps/2):,:]


    B_phi =      Bx[int(yFieldMapSteps/2):,:]
    dEzdt =   dEzdt[int(yFieldMapSteps/2):,:]
    dErhodt = dEydt[int(yFieldMapSteps/2):,:]

    dEzdt = dEzdt[1:,:]
    dEzdt = (dEzdt[:,1:] + dEzdt[:,:-1])/2
    dErhodt = dErhodt[1:,:]
    dErhodt = (dErhodt[:,1:] + dErhodt[:,:-1])/2

    rho_d, Z_d, rotBrho, rotBz = rot(rho, Zcyl, B_phi)

    mu0 = 1.25663706e-6

    jrho = rotBrho/mu0 - eps0 * dErhodt
    jz =   rotBz/mu0 - eps0 * dEzdt


    ax = plt.subplot(2,2,4)
    plt.streamplot(Z_d,rho_d,jz,jrho, density=1.4, color = np.sqrt(jz*jz+jrho*jrho), cmap="Reds")
    cbar = plt.colorbar()
    cbar.ax.set_ylabel('J [A/m$^2$]')
    plt.clim(0, 6e7)
    plt.title("Current Density")
    plt.xlabel("z [m]")
    plt.ylabel("r [m]")
    plt.xlim([-zLim,zLim])
    plt.ylim([0,yLim])


    # ##########################
    # ### Current Density sigmas
    # ##########################

    # jz = np.abs(jz)

    # sigma_z_j = np.sqrt(np.mean(Z_d**2 * jz)/np.mean(jz))
    # sigma_rho_j = np.sqrt(np.mean(rho_d**2 * jz)/np.mean(jz))
    # j_max = np.sum(jz*rho_d)*dz*dy*2*np.pi

    # plt.text(0.05, 0.9, "$\\sigma_z (j_r) = {:.4e}$ m".format(sigma_z_j), transform=ax.transAxes, c="k")
    # plt.text(0.05, 0.83, "$\\sigma_r (j_r)= {:.4e}$ m".format(sigma_rho_j), transform=ax.transAxes, c="k")
    # plt.text(0.05, 0.76, "$j_{z, max} ="+"{:.4e}".format(j_max) + "$ C", transform=ax.transAxes, c="k")


    plotsDirectory = "Outputs/ProtonfieldsPlots"
    plt.savefig(plotsDirectory + f"/recording_{timeStepNum}.png")

    fig.clear()

import multiprocessing

with multiprocessing.Pool() as pool_obj:
    pool_obj.map(SavePlots, range(numOfTimeSteps-1))

