#include <string>
#include <iostream>
#include <filesystem>
#include <fstream>
#include <list>
#include <vector>

#include <TH2F.h>
#include <TCanvas.h>
#include <TMath.h>
#include <TPaletteAxis.h>

namespace fs = std::filesystem;

using namespace std;

double fileRecordPeriod = 40000;
double numOfIons = 50000;
double ionisationDuration = 86e-6; // s
double ionisationRate = 5.391E12;  // 1/s
double normalizationFactor = ionisationDuration/numOfIons*ionisationRate;

struct location
{
    double x;
    double y;
    double z;
};

class trajectoryIterator
{
public:
    bool isNotAllUsed = true;

    int currRecordNum = 0;
    string tajectoryDir;

    list<vector<location>> allCoors;
    list<int> initRecordNums;

    vector<location> ionsLocat;

    trajectoryIterator(string dir)
    {
        tajectoryDir = dir;
        reset();
    }

    int findMaxReading()
    {
        int maxReading = -1;

        auto itLoc = allCoors.begin();
        auto itRead = initRecordNums.begin();

        for(; itLoc != allCoors.end(); )
        {
            int potMaxRead = (*itLoc).size() + (*itRead);

            if(potMaxRead > maxReading) maxReading = potMaxRead;

            itRead++;
            itLoc++;
        }

        return maxReading;
    }

    void reset()
    {
        allCoors.clear();
        initRecordNums.clear();

        int maxPartNum = 0;

        string path = tajectoryDir;
        for (const auto & fileEntry : fs::directory_iterator(path))
        {

            cout << "reading " << fileEntry.path() << " file" << endl;

            maxPartNum++;

            ifstream file(fileEntry.path());

            string idle;
            int initRecord;

            file >> idle >> idle >> idle >> initRecord;
            initRecordNums.push_back(initRecord);

            double x;
            double y;
            double z;

            vector<location> locations;
            location currentLoc;


            while(true)
            {
                file >> x >> y >> z >> idle >> idle >> idle;
                if(!file.eof())
                {
                    currentLoc = {x,y,z};
                    locations.push_back(currentLoc);
                }
                else
                {
                    break;
                }
            }

            file.close();

            allCoors.push_back(locations);

        }


        ionsLocat.reserve(maxPartNum);

        currRecordNum = 0;
        isNotAllUsed = true;

    };

    const vector<location>& getIonLocations()
    {

        ionsLocat.clear();

        list<vector<location>>::iterator coorsAllIter = allCoors.begin();
        list<int>::iterator  initRecordIter = initRecordNums.begin();

        if (allCoors.begin() == allCoors.end()) isNotAllUsed = false;

        for(; coorsAllIter != allCoors.end(); )
        {

            int currentLocIdx = currRecordNum - (*initRecordIter);

            if(currentLocIdx < 0)
            {
                coorsAllIter++;
                initRecordIter++;
                continue;
            }
            else if( currentLocIdx >=  (*coorsAllIter).size())
            {
                auto coorIter2Del = coorsAllIter;
                auto recotrIter2Del = initRecordIter;

                coorsAllIter++;
                initRecordIter++;

                allCoors.erase(coorIter2Del);
                initRecordNums.erase(recotrIter2Del);
            }
            else
            {
                ionsLocat.push_back((*coorsAllIter)[currentLocIdx]);  
                coorsAllIter++;
                initRecordIter++;
            }

        }

        currRecordNum++;

        return ionsLocat;

    }

};

vector<vector<double>> calcDensityMesh(const vector<location>& locs, int rBins, int zBins, double rLim, double zLim, unsigned long long &ionNum)
{
    double dz = 2*zLim/zBins;
    double dr = rLim/rBins;

    vector<vector<double>> result(zBins);

    for(int i = 0; i < zBins; ++i)
    {
        result[i].resize(rBins, 0);
    }

    for(auto el: locs)
    {
        const double& z = el.z;
        double r = TMath::Sqrt(el.y*el.y + el.x*el.x);

        if(r >= rLim || z <= -zLim || z >= zLim)
        {
            //in case the point is outside the limits
            continue;
        }

        int idxZ = TMath::Floor((z + zLim)/dz);
        int idxR = TMath::Floor(r/dr);

        if(idxZ >= result.size() || idxR >= result[0].size())
        {
            cerr << "Outside the mesh indexes" << endl;
        }

        result[idxZ][idxR] += 1;
    }

    ionNum = 0;

    for(int idxR = 0; idxR < result[0].size(); ++idxR)
    {
        double V = TMath::Pi() * dr*dr*dz *(2*idxR+1);
        for(int idxZ = 0; idxZ < result.size(); ++idxZ)
        {
            ionNum += result[idxZ][idxR];
            result[idxZ][idxR] /= V;

            //Normalize by probability of creating electron-ion pair in a time period
            result[idxZ][idxR] *= normalizationFactor;
        }
    }

    return result;

}

int main()
{
    auto c = new TCanvas("c1", "canvas", 1200, 800);
    trajectoryIterator trajectoryHandles("../Trajectories");

    ofstream ionNumberFile("ionNumberFile.txt");

    while(trajectoryHandles.isNotAllUsed)
    {

        int recordingNum = trajectoryHandles.currRecordNum;

        const vector<location>& locs = trajectoryHandles.getIonLocations();

        if(recordingNum % 1000 == 0) 
        {
            cout << "Current record number is " << recordingNum << "| locs size = " << locs.size() << endl;
        }

        int zBins = 25;
        int rBins = 25;
        double zLim = 1.7;
        double rLim = 0.01;

        string title = string("Ions density [m^{-3}]. Time = ") + to_string(recordingNum*fileRecordPeriod*2e-4) + " ns";
        TH2F *hist = new TH2F("hist", title.c_str(), zBins, -zLim, zLim, rBins, 0, rLim);

        unsigned long long ionNum;

        vector<vector<double>> mesh = calcDensityMesh(locs, rBins, zBins, rLim, zLim, ionNum);

        for(int i=0; i < mesh.size(); ++i)
        {
            for(int j = 0; j < mesh[0].size(); ++j)
            {
                hist->SetBinContent(i+1,j+1, mesh[i][j]);
            }
        }

        hist->SetStats(false);
        hist->GetXaxis()->SetTitle("z [m]");
        hist->GetYaxis()->SetTitle("#rho [m]");
        hist->SetMinimum(-1E-10);
        hist->SetMaximum(2200E9);
        hist->Draw("colz");

        ionNumberFile << recordingNum*fileRecordPeriod*2e-4 << "\t" <<  ionNum << endl;

        string filename = string("../plots/") + to_string(recordingNum) + ".png";
        c->Print(filename.c_str());

        delete hist;

    }

    ionNumberFile.close();

    return 0;
}