import numpy as np
import matplotlib.pyplot as plt

zLim = 83928.375
rLim = 0.03

zDivs = 6000
rDivs = 3000

file = open("FieldMapTest.txt", "r")

z = np.linspace(-zLim, zLim, zDivs)
r = np.linspace(0, rLim, rDivs)

Z, R = np.meshgrid(z,r)

Bz = np.empty(Z.shape)
Br = np.empty(R.shape)

for ir in range(rDivs):
    for iz in range(zDivs):
        line = file.readline()
    
        if line=="": break 

        _,_,br,bz = line.split()

        Bz[ir,iz] = float(bz)
        Br[ir,iz] = float(br)

plt.streamplot(Z,R,Bz,Br, density=2.4, color = np.sqrt(Bz**2+Br**2))
cbar = plt.colorbar()
cbar.ax.set_ylabel('B [T]')
plt.xlabel("x [m]")
plt.ylabel("y [m]")


plt.show()