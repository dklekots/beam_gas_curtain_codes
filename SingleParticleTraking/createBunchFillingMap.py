import numpy as np



b72train = np.array([i*25 for i in range(72)], dtype=np.float16)

t3train = np.concatenate( (b72train, b72train + b72train[-1] + 9*25) )
t3train = np.concatenate( (t3train, b72train + t3train[-1] + 9*25 ) )

t4train = np.concatenate( (t3train, b72train + t3train[-1] + 9*25 ) )

t333train = np.concatenate( (t3train, t3train + t3train[-1] + 25 * 39 ) ) 
t333train = np.concatenate( (t333train, t3train + t333train[-1] + 25 * 39 ) )
t333train = t333train[:-12] # skip 12 bunches (12 bunches gap in PS)


t334train = np.concatenate( (t3train, t3train + t3train[-1] + 25 * 39 ) ) 
t334train = np.concatenate( (t334train, t4train + t334train[-1] + 25 * 39 ) )
t334train = t334train[:-12] # skip 12 bunches (12 bunches gap in PS)

complTrain = np.concatenate( (t333train, t334train + t333train[-1] + 25*40) )
complTrain = np.concatenate( (complTrain, t334train + complTrain[-1] + 25*40) )
complTrain = np.concatenate( (complTrain, t334train + complTrain[-1] + 25*40) )

# shift the time in such a way that the centre of the beam will be in the centre of 25 ns 
complTrain = complTrain + 25.0/2.0

#convert fron nanoseconds to seconds
complTrain = complTrain/1e9


print("{", complTrain[0], end="")
for i in range(1,complTrain.shape[0]):
    print(",", complTrain[i], end="")
print("}")

print("\nThe total number of elements in array is", complTrain.shape[0])