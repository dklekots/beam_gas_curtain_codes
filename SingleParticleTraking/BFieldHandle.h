#ifndef __BFIELDHANDLE__H__
#define __BFIELDHANDLE__H__

struct BFieldHandleObj
{

    double mapStep;

    double xMinPoint;
    double yMinPoint;
    double zMinPoint;

    double xMaxPoint;
    double yMaxPoint;
    double zMaxPoint;

    int xIdxSize;
    int yIdxSize;
    int zIdxSize;

    //indexes [xIdx][yIdx][zIdx]
    double*** BxMap;
    double*** ByMap;
    double*** BzMap;

};

typedef struct BFieldHandleObj BFieldHandle;

/*
    The filename should be in .csv format with the columns
    x, y, z, Bx, By, Bz

    mapStep is the distance between the points in matric in millimeters

    Where x y z represent coordinates of the point in millimetres
    Bx By Bx represent the magnetic field in Tesla

    First line of the file will be ignored (it can contain headers)
*/
BFieldHandle* LoadFieldMap(const char* filename, double mapStep);

void deleteBfieldHandle(BFieldHandle*);


/*
    x, y, z shall be in meters
 */
void GetBfield(const BFieldHandle* aHandle, const double* restrict x, const double* restrict y, const double* restrict z,
                 double* restrict eBx, double * restrict eBy, double * restrict eBz, int* restrict ierr);



//====================================================================//
//                2D FIELD MAP


struct BFieldHandleObj2D
{

    double dr;
    double dz;

    double rMinPoint;
    double zMinPoint;

    double rMaxPoint;
    double zMaxPoint;

    int rIdxSize;
    int zIdxSize;

    //indexes [xIdx][yIdx]
    double** BrMap;
    double** BzMap;

};

typedef struct BFieldHandleObj2D BFieldHandle2D;

/*
    The filename should be in .csv format with the columns
    r, z, Br, Bz

    dr and az are the distance between the points in meters

    Where r z represent coordinates of the point in meters
    Br Bz represent the magnetic field in Tesla

    First line of the file will be ignored (it can contain headers)
*/
BFieldHandle2D* LoadFieldMap2D(const char* filename, double dr, double dz);

void deleteBfieldHandle2D(BFieldHandle2D* aHandle);

/*
    x, y shall be in meters
 */
void GetBfield2D(const BFieldHandle2D* aHandle, const double* restrict r, const double* restrict z,
                 double* restrict eBr, double * restrict eBz, int* restrict ierr);


#endif //__BFIELDHANDLE__H__
