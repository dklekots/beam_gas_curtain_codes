import numpy as np
import matplotlib.pyplot as plt
import matplotlib.axes


file = open("fieldValidationFile.txt")

file.readline() # skip first line

startCoor = -0.007    #one should know thease values from how the file was generated
endCoor   =  0.007
numOfVivisions = 1000

x = np.linspace(startCoor, endCoor, numOfVivisions)
y = np.linspace(startCoor, endCoor, numOfVivisions)

X,Y = np.meshgrid(x, y)

Bx = np.empty(X.shape)
By = np.empty(Y.shape)

Ex = np.empty(X.shape)
Ey = np.empty(Y.shape)

for i in range(numOfVivisions):
    for j in range(numOfVivisions):

        linesplit = file.readline().split()

        # one need to know that x correspond to x and j to y (form the creation of imput file)

        Bx[j][i] = float(linesplit[3])
        By[j][i] = float(linesplit[4])

        Ex[j][i] = float(linesplit[6])
        Ey[j][i] = float(linesplit[7])



plt.subplot(1,2,1)
plt.streamplot(X,Y,Bx,By, density=2.4, color = np.sqrt(Bx**2+By**2), cmap="cool")
plt.xlim((startCoor, endCoor))
plt.ylim((startCoor, endCoor))
plt.title("Magnetic Field")
plt.xlabel("x [m]")
plt.ylabel("y [m]")
cbar = plt.colorbar()
cbar.ax.set_ylabel('B [T]')
cbar.formatter.set_powerlimits((3, 1))

plt.subplot(1,2,2)
plt.streamplot(X,Y,Ex,Ey, density=2.4, color = (Ex*X+Ey*Y)/np.sqrt(X*X+Y*Y), cmap="seismic")
plt.xlim((startCoor, endCoor))
plt.ylim((startCoor, endCoor))
plt.title("Electric Field")
plt.xlabel("x [m]")
plt.ylabel("y [m]")
cbar = plt.colorbar()
cbar.ax.set_ylabel('E [V/m]')
cbar.formatter.set_powerlimits((3, 1))
eLimit = np.max(np.abs((Ex*X+Ey*Y)/np.sqrt(X*X+Y*Y)))
plt.clim(-eLimit,eLimit)



#####################################
## 2D Ploats
#####################################

rho = X[500,500:]
B_phi = By[500,500:]

fig2D, ((ax2D1, ax2D2), (ax2D3, ax2D4)) = plt.subplots(2,2,)

ax2D1.plot(rho, B_phi)
ax2D1.set_xlabel("r [m]")
ax2D1.set_ylabel("B_phi [T]")







plt.show()