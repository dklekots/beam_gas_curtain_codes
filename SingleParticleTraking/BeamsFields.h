#ifndef __BEAMSFIELDS_H__
#define __BEAMSFIELDS_H__

#include "BFieldHandle.h"

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

void CalcElectrBeamFields(const double* time, const double* restrict x, const double* restrict y, const double* restrict z,
                           double* restrict Bx, double* restrict By, double* restrict Bz,
                           double* restrict Ex, double* restrict Ey, double* restrict Ez);

void CalcProtBeamFields(const double* time, const double* restrict x, const double* restrict y, const double* restrict z,
                        double* restrict Bx_out, double* restrict By_out, double* restrict Bz_out,
                        double* restrict Ex_out, double* restrict Ey_out, double* restrict Ez_out, int* restrict ierr,
                        BFieldHandle2D* restrict bunchFieldHandle);

#endif //__BEAMSFIELDS_H__