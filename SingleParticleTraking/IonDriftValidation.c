#include "BFieldHandle.h"
#include "RungeCutta.h"
#include "BeamsFields.h"

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int initStep = 125000;
double stepTime = 2e-13;
int numOfSteps = 2*125000;
int fileRecordPeriod = 100;

const double zFieldMapSteps = 2000;
const double rFieldMapSteps = 100;
const double zLim = 1.7;  //m
const double rLim = 0.005;  //m


struct interactionFuncOptsObj
{
    double charge; //in elementary charges
    double mass; //kg
    BFieldHandle* guidingBfield;
    BFieldHandle2D* bunchEfield;
    FILE* outFile;

};
typedef struct interactionFuncOptsObj interactionFuncOpts;

void CalculateInteractions(const int* restrict n, const double *restrict x, double *restrict f, 
                           const double* restrict cuttentTime, void* restrict funcOptsVoid, 
                           int* restrict ierr)
{
    *ierr = 0;
    interactionFuncOpts* funcOpts = (interactionFuncOpts*)funcOptsVoid;

    const double e = 1.60217663E-19; //coulomb

    const double *xCoor = x;
    const double *yCoor = x + 1;
    const double *zCoor = x + 2;

    double r = sqrt(xCoor[0]*xCoor[0]+yCoor[0]*yCoor[0]);
    if(r > 0.03 || abs(zCoor[0]) > 2)
    {
        *ierr = 1; //the flag means we are outside deevice
        return;
    }

    const double *xDot = x + 3;
    const double *yDot = x + 4;
    const double *zDot = x + 5;


    double* fx = f;
    double* fy = f + 1;
    double* fz = f + 2;

    double* fxDot = f + 3;
    double* fyDot = f + 4;
    double* fzDot = f + 5;

    // interaction with magnetic field and fx, fy, fz
    *fx = *xDot;
    *fy = *yDot;
    *fz = *zDot;

    double eBx=0;
    double eBy=0;
    double eBz=0;
    
    double eEx=0;
    double eEy=0;
    double eEz=0;

    double tmpFex, tmpFey, tmpFez, tmpFbx, tmpFby, tmpFbz;

    BFieldHandle* fieldHangle = funcOpts->guidingBfield;
    GetBfield(fieldHangle, xCoor, yCoor, zCoor, &tmpFbx, &tmpFby, &tmpFbz, ierr);
    if (*ierr > 0)
    {
        return; //no reason to further calculate if the particle is outside the field map
    }
    eBx+=tmpFbx;
    eBy+=tmpFby;
    eBz+=tmpFbz;

    CalcElectrBeamFields(cuttentTime, xCoor, yCoor, zCoor, &tmpFbx, &tmpFby, &tmpFbz, &tmpFex, &tmpFey, &tmpFez);
    eBx+=tmpFbx;
    eBy+=tmpFby;
    eBz+=tmpFbz;
    eEx+=tmpFex;
    eEy+=tmpFey;
    eEz+=tmpFez;

    BFieldHandle2D* bunchFieldHandle = funcOpts->bunchEfield;
    CalcProtBeamFields(cuttentTime, xCoor, yCoor, zCoor, &tmpFbx, &tmpFby, &tmpFbz, &tmpFex, &tmpFey, &tmpFez, ierr, bunchFieldHandle);
    if (*ierr > 0)
    {
        return; //no reason to further calculate if the particle is outside the field map
    }  
    eBx+=tmpFbx;
    eBy+=tmpFby;
    eBz+=tmpFbz;
    eEx+=tmpFex;
    eEy+=tmpFey;
    eEz+=tmpFez;


    eBx*=e;
    eBy*=e;
    eBz*=e;

    eEx*=e;
    eEy*=e;
    eEz*=e;


    double pChar = funcOpts->charge;
    double particleMasses = funcOpts->mass;

    *fxDot = pChar*(yDot[0]*eBz - zDot[0]*eBy + eEx)/particleMasses;
    *fyDot = pChar*(zDot[0]*eBx - xDot[0]*eBz + eEy)/particleMasses;
    *fzDot = pChar*(xDot[0]*eBy - yDot[0]*eBx + eEz)/particleMasses;

    static int callNumber = 0;

    if (callNumber % (4*fileRecordPeriod) == 0)
    {
        callNumber = 0;
        double forcex = pChar*(yDot[0]*eBz - zDot[0]*eBy + eEx);
        double forcey = pChar*(zDot[0]*eBx - xDot[0]*eBz + eEy);
        double forcez = pChar*(xDot[0]*eBy - yDot[0]*eBx + eEz);

        fprintf(funcOpts->outFile, "%.15e %.15e %.15e ", forcex, forcey, forcez);
    }

    callNumber++;

}



int main(int* argc, char **argv)
{

    BFieldHandle2D* bunchFieldHandle;
    bunchFieldHandle = LoadFieldMap2D("Proton bunch field map.csv", 6.012024048096192e-05, 168.02477477477805);

    BFieldHandle* fieldHangle;
    const char* BfieldMapFileName = "B-Field [Ms] sorted.csv";
    fieldHangle = LoadFieldMap(BfieldMapFileName, 10);

    int nTrackPars = 6;

    const char* trakingOutFileName = "Outputs/IonDriftValidation/Tracks.txt";

    double x0[] = {0.003, 0.001, 0, -800, 0, 0};

    FILE *outFile = fopen(trakingOutFileName, "w");
    fprintf(outFile, "Init recording number %d\n", initStep/fileRecordPeriod); 

    interactionFuncOpts ionInteractProperties = { 1, 3.350967843e-26, fieldHangle, bunchFieldHandle, outFile};
    void* ionInteractPropertiesVoid = &ionInteractProperties;

    calcRungeKutta(CalculateInteractions, &nTrackPars, x0, &numOfSteps, &initStep, &stepTime, outFile, &fileRecordPeriod, ionInteractPropertiesVoid);
    fclose(outFile); 

    deleteBfieldHandle(fieldHangle);

    ////////////////////////////
    // Proton beam field
    ////////////////////////////

    #pragma omp parallel for default(shared) schedule(dynamic)
    for(int timeStepNum=initStep; timeStepNum <= numOfSteps; timeStepNum+=fileRecordPeriod)
    {
        
        char filename[256];
        sprintf(filename, "Outputs/IonDriftValidation/ProtonFields/timeStepNum%d.txt",timeStepNum);
        FILE* mapFile = fopen(filename, "w");

        double Ex1, Ey1, Ez1, Ex2, Ey2, Ez2;
        double Bx1, By1, Bz1, Bx2, By2, Bz2;

        double currentTime = timeStepNum*stepTime;


        for(int iz = 0; iz < zFieldMapSteps; ++iz)
        {

            double z = iz*2*zLim/(zFieldMapSteps-1) - zLim;

            for(int ir = 0; ir < rFieldMapSteps; ++ir)
            {
                double y = ir*rLim/(rFieldMapSteps-1);
                double x = 0;

                int ierr;   
                CalcProtBeamFields(&currentTime, &x, &y, &z, &Bx1, &By1, &Bz1, &Ex1, &Ey1, &Ez1, &ierr, bunchFieldHandle);
                CalcElectrBeamFields(&currentTime, &x, &y, &z, &Bx2, &By2, &Bz2, &Ex2, &Ey2, &Ez2);

                double Bx = Bx1+Bx2;
                double By = By1+By2;
                double Bz = Bz1+Bz2;

                double Ex = Ex1+Ex2;
                double Ey = Ey1+Ey2;
                double Ez = Ez1+Ez2;

                fprintf(mapFile, "%e,%e,%e,%e,%e,%e,%e,%e,%e\n", x, y, z, Bx, By, Bz, Ex, Ey, Ez);
            }

        }


        fclose(mapFile);
    }

    deleteBfieldHandle2D(bunchFieldHandle);

    return 0;
}