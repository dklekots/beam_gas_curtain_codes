import numpy as np
import matplotlib.pyplot as plt

zLim = 2
yLim = 0.03

zDivs = 10000
yDivs = 100

file = open("FieldMapTest.txt", "r")

z = np.linspace(-zLim, zLim, zDivs)
y = np.linspace(-yLim, yLim, yDivs)

Z, Y = np.meshgrid(z,y)

Bz = np.empty(Z.shape)
By = np.empty(Y.shape)

for iz in range(zDivs):
    for iy in range(yDivs):

        line = file.readline()

        _,_,_,_,by,bz = line.split()

        Bz[iy,iz] = float(bz)
        By[iy,iz] = float(by)

plt.streamplot(Z,Y,Bz,By, density=2.4, color = np.sqrt(Bz**2+By**2), cmap="Reds")
cbar = plt.colorbar()
cbar.ax.set_ylabel('B [T]')
plt.xlabel("x [m]")
plt.ylabel("y [m]")


plt.show()