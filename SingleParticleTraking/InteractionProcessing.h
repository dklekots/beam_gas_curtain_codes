#ifndef __InteractionProcessing_h__
#define __InteractionProcessing_h__

#include "BFieldHandle.h"

struct interactionFuncOptsObj
{
    double charge; //in elementary charges
    double mass; //kg
    BFieldHandle* guidingBfield;
    BFieldHandle2D* bunchEfield;

};
typedef struct interactionFuncOptsObj interactionFuncOpts;

void CalculateInteractions(const int* restrict n, const double *restrict x, double *restrict f, 
                           const double* restrict cuttentTime, void* restrict funcOpts, 
                           int* restrict ierr);

#endif //__InteractionProcessing_h__