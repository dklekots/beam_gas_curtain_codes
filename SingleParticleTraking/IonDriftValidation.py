import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors

matplotlib.use('Agg') # this line is needed for threads safety in using matplotlib

import os
import numpy as np


zLim = 1.7
rLim = 0.005
stepTime = 2e-13
fileRecordPeriod = 100
initialRecorfNum = 1250

zFieldMapSteps = 2000
yFieldMapSteps = 100


z = np.linspace(-zLim, zLim, zFieldMapSteps)
y = np.linspace(0, rLim, yFieldMapSteps)

Z,Y = np.meshgrid(z,y)



Ez = np.empty((yFieldMapSteps, zFieldMapSteps))
Ey = np.empty((yFieldMapSteps, zFieldMapSteps))


fig = plt.figure(figsize=(15, 10))

def SavePlots(args):

    recordingNum, line = args
    lineSplit = line.split()

    file = open(f"Outputs/IonDriftValidation/ProtonFields/timeStepNum{ (recordingNum +initialRecorfNum)*fileRecordPeriod }.txt")

    for iz in range(zFieldMapSteps):
        for iy in range(yFieldMapSteps):

            lineField = file.readline()
            lineField = lineField.split(",")

            Ez[iy,iz] = float(lineField[8])
            Ey[iy,iz] = float(lineField[7])

    file.close()    

    plt.streamplot(Z,Y,Ez,Ey, density=2.4, color = np.sqrt(Ez*Ez+Ey*Ey), norm=colors.LogNorm(vmin=1, vmax=7E8))
    cbar = plt.colorbar()
    plt.clim(1, 7e8)
    cbar.ax.set_ylabel('E [V/m]')
    plt.xlabel("z [m]")
    plt.ylabel("y [m]")
    plt.xlim([-zLim,zLim])
    plt.ylim([0,rLim])

    #######################################
    ## plot vector of force
    #######################################

    print("Current recording is", recordingNum)
        
    fx = float(lineSplit[0])
    fy = float(lineSplit[1])
    fz = float(lineSplit[2])

    x = float(lineSplit[3])
    y = float(lineSplit[4])
    z = float(lineSplit[5])
    
    r  = np.sqrt(x**2  + y**2 )

    fr = (fx*x + fy*y)/r
    fNorm  = np.sqrt(fr**2/(rLim)**2 + fz**2/zLim**2)
    f = np.sqrt(fr**2 + fz**2)

    plt.scatter(z,r, marker="o", color="r")
    plt.xlabel("z [m]")
    plt.ylabel("$\\rho$ [m]")
    plt.xlim([-zLim, zLim])
    plt.ylim([0, rLim])
    dz = fz/fNorm*0.1
    dr = fr/fNorm*0.1
    ax = plt.gcf().axes[0]
    ax.annotate('', xy=(z + dz, r + dr), xytext=(z + dz*0.1, r + dr*0.1), arrowprops=dict(facecolor='black', shrink=0.0005))
    
    plt.text(0.65, 0.95, "F = {:.4E} [N]".format(f), transform=ax.transAxes, fontsize=20)
    plt.title("time = {:.4f} ns".format(fileRecordPeriod*stepTime*1e9*(recordingNum + initialRecorfNum)))

    #proton bunch
    plt.scatter([-0.3*(fileRecordPeriod*stepTime*1e9*(recordingNum+initialRecorfNum) - 37.5)], [0.00008], marker="o", color="orange")

    Zi = np.argmin(np.abs(Z[0,:] - z))
    Yi = np.argmin(np.abs(Y[:,0] - r))
    E = np.sqrt( Ez[Yi,Zi]**2 + Ey[Yi,Zi]**2 )
    plt.text(0.65, 0.9, "E = {:.4E} [V/m]".format(E), transform=ax.transAxes, fontsize=20)
    


    plotsDirectory = "Outputs/IonDriftValidation/plots"
    plt.savefig(plotsDirectory + f"/recording_{recordingNum+1}.png")

    fig.clear()



import multiprocessing

file = open("Outputs/IonDriftValidation/Tracks.txt")

next(file) # skip header line

with multiprocessing.Pool() as pool_obj:
    pool_obj.map(SavePlots, enumerate(file))

##########################################
### Creating Video
##########################################


import moviepy.video.io.ImageSequenceClip
plotsDirectory='Outputs/IonDriftValidation/plots'
fps=64

image_files = [os.path.join(plotsDirectory, filename) for filename in\
               sorted(os.listdir(plotsDirectory), key=lambda x: int("".join([i for i in x if i.isdigit()])))]
clip = moviepy.video.io.ImageSequenceClip.ImageSequenceClip(image_files, fps=fps)
clip.write_videofile('Outputs/IonDriftValidation/video.mp4')
