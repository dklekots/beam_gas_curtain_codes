import numpy as np
import matplotlib.pyplot as plt
from scipy.special import erf

def CalcBunchEField(rho, z):

    Q = 2.2e11 * 1.6e-19
    eps0 = 8.8541878128e-12

    sigma =  306e-6
    sigma_z = 8.3e-2

    R=np.sqrt(rho*rho + sigma*sigma/(sigma_z*sigma_z)*z*z)
    K = Q/(eps0*(2*np.pi)**1.5 * R*R) * sigma / sigma_z
    E = np.sqrt(np.pi/2)*erf(R/sigma/1.414213562) - R/sigma/np.exp(R*R/sigma/sigma/2)
    E = E*K


    E_rho = E*rho/R
    E_z   = E*  z/R

    return E_rho, E_z

zLim = 8.3e-2*3  #m
rLim = 306e-6*3  #m
zFieldMapSteps = 20000
rFieldMapSteps = 500

dz = 2*zLim/(zFieldMapSteps-1)
dr = rLim/(rFieldMapSteps-1)

z = np.linspace(-zLim, zLim, zFieldMapSteps)
rho = np.linspace(0, rLim, rFieldMapSteps)

Z,Rho = np.meshgrid(z,rho)

E_rho, E_z = CalcBunchEField(Rho, Z)

#######################
### Electric field
#######################

plt.subplot(2,2,1)
plt.streamplot(Z,Rho,E_z,E_rho, density=2.4, color = np.sqrt(E_z*E_z+E_rho*E_rho), cmap="seismic")
cbar = plt.colorbar()
cbar.ax.set_ylabel('E [V/m]')
plt.title("E field")
plt.xlabel("z [m]")
plt.ylabel("r [m]")
# plt.xlim([-zLim,zLim])
plt.ylim([0,rLim])


################################
### Charge Density from E field
################################

div_rho = (E_rho[1:,:]*Rho[1:, :] - E_rho[:-1,:]*Rho[:-1,:])/dr / (Rho[1:,:] - dr/2)
div_rho = div_rho[:,1:]
div_z = (E_z[:, 1:] - E_z[:, :-1]) / dz
div_z = div_z[1:,:]

eps0 = 8.8541878128e-12
chargeDensity = (div_rho + div_z) * eps0

ax = plt.subplot(2,2,2)
plt.contourf(Z[1:, 1:] - dz/2, Rho[1:, 1:] - dr/2, chargeDensity, 20) 
cbar = plt.colorbar()
cbar.ax.set_ylabel('$\\rho$ [C/m$^3$]')
plt.xlabel("z [m]")
plt.ylabel("r [m]")
plt.title("Charge Density $\\rho = \\varepsilon_0 div(\\vec{E})$")

##########################
### Charge Density sigmas
##########################

Z4sigma = Z[1:, 1:] - dz/2
rho4sigma = Rho[1:, 1:] - dr/2

sigma_z = np.sqrt(np.mean(Z4sigma**2 * chargeDensity)/np.mean(chargeDensity))
sigma_rho = np.sqrt(np.mean(rho4sigma**2 * chargeDensity)/np.mean(chargeDensity))
Q = np.sum(chargeDensity*rho4sigma)*dz*dr*2*np.pi

plt.text(0.05, 0.9, "$\\sigma_z = {:.4e}$ m".format(sigma_z), transform=ax.transAxes, c="w")
plt.text(0.05, 0.83, "$\\sigma_r = {:.4e}$ m".format(sigma_rho), transform=ax.transAxes, c="w")
plt.text(0.05, 0.76, "$Q = {:.4e}$ C".format(Q), transform=ax.transAxes, c="w")

################################
### Charge Density from model
################################

Q_model = 2.2e11 * 1.6e-19
sigma_model =  306e-6
sigma_z_model = 8.3e-2

chargeDensity_model = Q_model/( (2*np.pi)**1.5 * sigma_model**2 *  
                               sigma_z_model) * np.exp(-Rho**2/(2*sigma_model**2) - 
                                                     Z**2/(2*sigma_z_model**2))

ax = plt.subplot(2,2,4)
plt.contourf(Z, Rho, chargeDensity_model, 20) 
cbar = plt.colorbar()
cbar.ax.set_ylabel('$\\rho$ [C/m$^3$]')
plt.xlabel("z [m]")
plt.ylabel("r [m]")
plt.title("Model Charge Density")

Q_model = np.sum(chargeDensity_model*Rho)*dz*dr*2*np.pi

plt.text(0.05, 0.9, "$\\sigma_z = {:.4e}$ m".format(sigma_z_model), transform=ax.transAxes, c="w")
plt.text(0.05, 0.83, "$\\sigma_r = {:.4e}$ m".format(sigma_model), transform=ax.transAxes, c="w")
plt.text(0.05, 0.76, "$Q = 3.25e-08$ C", transform=ax.transAxes, c="w")


plt.show()