#include <stdio.h>
#include "BFieldHandle.h"


int main(int argc, char** argv)
{


    const char* BfieldMapFileName = "B-Field [Ms] sorted.csv";
    BFieldHandle* fieldHangle = fieldHangle = LoadFieldMap(BfieldMapFileName, 10);

    double zLim = 2;
    double yLim = 0.03;

    int zDivs = 10000;
    int yDivs = 100;

    FILE* file = fopen("FieldMapTest.txt", "w");

    for(int iy = 0; iy < yDivs; ++iy)
    {
        double y = 2*yLim/(yDivs-1)*iy - yLim;

        for(int iz = 0; iz < zDivs; ++iz)
        {
            double z = 2*zLim/(zDivs-1)*iz - zLim;
            double x = 0.01;

            int ierr;

            double Bx, By, Bz;

            GetBfield(fieldHangle, &x, &y, &z, &Bx, &By, &Bz, &ierr);

            if(ierr > 0) Bx = By = Bz = 0;

            fprintf(file, "%e %e %e %e %e %e \n", x, y, z, Bx, By, Bz);

        }
    }

    fclose(file);
    deleteBfieldHandle(fieldHangle);

    return 0;
}