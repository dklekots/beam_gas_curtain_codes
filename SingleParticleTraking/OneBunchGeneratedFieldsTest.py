import numpy as np
import matplotlib.pyplot as plt
from scipy.special import erf

dt = 5e-13 #s
zLim = 8.3e-2*3  #m
rhoLim = 306e-6*3  #m
zFieldMapSteps = 20000
rhoFieldMapSteps = 1000
numOfTimeSteps = 50001 #coresponds to 25 ns

timeStepNum = 25000

dz = 2*zLim/(zFieldMapSteps-1)
drho = rhoLim/(rhoFieldMapSteps-1)

z = np.linspace(-zLim, zLim, zFieldMapSteps)
rho = np.linspace(0, rhoLim, rhoFieldMapSteps)

Z,Rho = np.meshgrid(z,rho)

gamma = 7460.3
eps0 = 8.8541878128e-12
mu0 = 1.25663706e-6

def CalcOneBunchEField(rho, z):

    Q = 2.2e11 * 1.6e-19

    sigma =  306e-6
    sigma_z = 8.3e-2*gamma

    R=np.sqrt(rho*rho + sigma*sigma/(sigma_z*sigma_z)*z*z)
    K = Q/(eps0*(2*np.pi)**1.5 * R*R) * sigma / sigma_z
    E = np.sqrt(np.pi/2)*erf(R/sigma/1.414213562) - R/sigma/np.exp(R*R/sigma/sigma/2)
    E = E*K


    E_rho = E*rho/R
    E_z   = E*  z/R

    return E_rho, E_z

def CalcProtBeamFields(t, Rho, Z):

    v = -3e8 # m/s
    t0 = 12.5E-9 # s

    Zprim = gamma*(Z - v*(t-t0))

    E_rho_prin, E_z_prin = CalcOneBunchEField(Rho, Zprim)

    E_z   = E_z_prin
    E_rho = gamma*E_rho_prin
    B_phi = gamma*E_rho_prin*v/9e16

    return E_z, E_rho, B_phi


E_z, E_rho, B_phi = CalcProtBeamFields(timeStepNum*dt, Rho, Z)
E_z_nt, E_rho_nt, B_phi_nt = CalcProtBeamFields((timeStepNum+0.5)*dt, Rho, Z)
E_z_pw, E_rho_pw, B_phi_pw = CalcProtBeamFields((timeStepNum-0.5)*dt, Rho, Z)

#############################################################################

def div(Rho, Z, Frho, Fz):

    div_rho = 1/(Rho[1:,:] - drho/2) * ( (Rho*Frho)[1:,:] - (Rho*Frho)[:-1,:] ) / drho
    div_z =  (Fz[:,1:] - Fz[:,:-1])/dz

    div_rho = div_rho[:,1:]
    div_z = div_z[1:,:]

    return Rho[1:,1:] - drho/2, Z[1:,1:] - dz/2, div_rho + div_z

def rot(Rho, Z, Fphi):

    rot_rho = -1/(Rho[:,1:] - drho/2) * ( (Rho*Fphi)[:,1:] - (Rho*Fphi)[:,:-1] ) / dz
    rot_z = (Fphi[1:,:] - Fphi[:-1,:])/drho

    rot_rho = (rot_rho[1:,:] + rot_rho[:-1,:])/2
    rot_z = (rot_z[:,1:] + rot_z[:,:-1])/2

    return Rho[1:,1:] - drho/2, Z[1:,1:] - dz/2, rot_rho, rot_z

#######################
### E Field
#######################

plt.figure(figsize=(15, 10))
plt.subplot(2,2,1)
plt.streamplot(Z,Rho,E_z,E_rho, density=2.4, color = np.sqrt(E_z**2+E_rho**2), cmap="Reds")
cbar = plt.colorbar()
cbar.ax.set_ylabel('E [V/m]')
plt.clim(0, 7e8)
plt.title("E field. Time = {:.4f} ns".format(timeStepNum*dt*1e9))
plt.xlabel("z [m]")
plt.ylabel("y [m]")
plt.xlim([-zLim,zLim])
plt.ylim([0,rhoLim])

#######################
### charge Density
#######################

Rho_d, Z_d, chDen = div(Rho, Z, E_rho, E_z)
chDen*=eps0


ax = plt.subplot(2,2,2)
plt.contourf(Z_d, Rho_d, chDen, 20) 
cbar = plt.colorbar()
cbar.ax.set_ylabel('$\\rho$ [C/m$^3$]')
plt.clim(0, 0.28)
plt.xlabel("z [m]")
plt.ylabel("r [m]")
plt.title("Charge Density")


##########################
### Charge Density sigmas
##########################

sigma_z = np.sqrt(np.mean(Z_d**2 * chDen)/np.mean(chDen))
sigma_rho = np.sqrt(np.mean(Rho_d**2 * chDen)/np.mean(chDen))
Q = np.sum(chDen*Rho_d)*dz*drho*2*np.pi

plt.text(0.05, 0.9, "$\\sigma_z = {:.4e}$ m".format(sigma_z), transform=ax.transAxes, c="w")
plt.text(0.05, 0.83, "$\\sigma_r = {:.4e}$ m".format(sigma_rho), transform=ax.transAxes, c="w")
plt.text(0.05, 0.76, "$Q = {:.4e}$ C".format(Q), transform=ax.transAxes, c="w")

#######################
### B Field
#######################

ax = plt.subplot(2,2,3)
plt.contourf(Z, Rho, -B_phi, 20) 
cbar = plt.colorbar()
cbar.ax.set_ylabel('|$B_{\\varphi}$| [T]')
plt.clim(0, 0.009)
plt.xlabel("z [m]")
plt.ylabel("y [m]")
plt.title("Magnetic field")

#######################
### Current Density
#######################

dEzdt = (E_z_nt - E_z_pw)/dt
dEzdt = (dEzdt[1:,:] + dEzdt[:-1,:])/2
dEzdt = (dEzdt[:,1:] + dEzdt[:,:-1])/2

dErhodt = (E_rho_nt - E_rho_pw)/dt
dErhodt = (dErhodt[1:,:] + dErhodt[:-1,:])/2
dErhodt = (dErhodt[:,1:] + dErhodt[:,:-1])/2

Rho_d, Z_d, rotB_rho, rotB_z = rot(Rho, Z, B_phi)

j_rho = rotB_rho/mu0 - dErhodt*eps0
j_z   = rotB_z/mu0   - dEzdt*eps0

ax = plt.subplot(2,2,4)
plt.streamplot(Z_d,Rho_d,j_z,j_rho, density=1.4, color = np.sqrt(j_z**2+j_rho**2), cmap="Reds")
cbar = plt.colorbar()
cbar.ax.set_ylabel('J [A/m$^2$]')
plt.clim(0, 8.4e7)
plt.title("Current Density")
plt.xlabel("z [m]")
plt.ylabel("r [m]")
plt.xlim([-zLim,zLim])
plt.ylim([0,rhoLim])


##########################
### Current Density sigmas
##########################

j_z = np.abs(j_z)

sigma_z_j = np.sqrt(np.mean(Z_d**2 * j_z)/np.mean(j_z))
sigma_rho_j = np.sqrt(np.mean(Rho_d**2 * j_z)/np.mean(j_z))
j_max = np.sum(j_z*Rho_d)*dz*drho*2*np.pi

plt.text(0.05, 0.9, "$\\sigma_z (j_r) = {:.4e}$ m".format(sigma_z_j), transform=ax.transAxes, c="k")
plt.text(0.05, 0.83, "$\\sigma_r (j_r)= {:.4e}$ m".format(sigma_rho_j), transform=ax.transAxes, c="k")
plt.text(0.05, 0.76, "$j_{z, max} ="+"{:.4e}".format(j_max) + "$ C", transform=ax.transAxes, c="k")


plt.show()