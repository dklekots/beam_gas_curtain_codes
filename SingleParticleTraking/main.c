// run 'gcc main.c BFieldHandle.h BFieldHandle.c RungeCutta.h RungeCutta.c InteractionProcessing.h InteractionProcessing.c BeamsFields.c BeamsFields.h -lm -fopenmp'
// to compile this code

#include <stdio.h>

#include "BFieldHandle.h"

#include "InteractionProcessing.h"

#include "RungeCutta.h"

int main(int *argc, char **argv)
{

    BFieldHandle2D* bunchFieldHandle;
    bunchFieldHandle = LoadFieldMap2D("Proton bunch field map.csv", 6.012024048096192e-05, 168.02477477477805);

    // ///////////////////////////////////////
    // // Proton beam field validation
    // ///////////////////////////////////////     

    // const double validationTimeStep = 5e-12; //s
    // const double zLim = 8.3e-2*3;  //m
    // const double yLim = 306e-6*3;  //m
    // // const double zLim = 0.155;  //m
    // // const double yLim = 0.007;  //m
    // // const double zLim = 11.25;  //m
    // // const double yLim = 0.03;   //m
    // const double zFieldMapSteps = 2000;
    // const double yFieldMapSteps = 100;
    // const int numOfTimeSteps = 5001; //coresponds to 25 ns

    // #pragma omp parallel for default(shared) schedule(dynamic)
    // for(int timeStepNum=0; timeStepNum <= numOfTimeSteps; ++timeStepNum)
    // {
        
    //     char filename[256];
    //     sprintf(filename, "Outputs/ProtonFields/timeStepNum%d.txt",timeStepNum);
    //     FILE* mapFile = fopen(filename, "w");

    //     double Ex, Ey, Ez;
    //     double Bx, By, Bz;

    //     double currentTime = timeStepNum*validationTimeStep;


    //     for(int iz = 0; iz < zFieldMapSteps; ++iz)
    //     {

    //         double z = iz*2*zLim/(zFieldMapSteps-1) - zLim;

    //         for(int iy = 0; iy < yFieldMapSteps; ++iy)
    //         {
    //             double y = iy*2*yLim/(yFieldMapSteps-1) - yLim;
    //             double x = 0;

    //             int ierr;           
    //             CalcProtBeamFields(&currentTime, &x, &y, &z, &Bx, &By, &Bz, &Ex, &Ey, &Ez, &ierr, bunchFieldHandle);

    //             fprintf(mapFile, "%e,%e,%e,%e,%e,%e,%e,%e,%e\n", x, y, z, Bx, By, Bz, Ex, Ey, Ez);
    //         }

    //     }


    //     fclose(mapFile);
    // }


    ///////////////////////////////////////
    // Traking
    ///////////////////////////////////////   

    const int nParticles = 50000; // number of particles of one charge sign

    BFieldHandle* fieldHangle;
    const char* BfieldMapFileName = "B-Field [Ms] sorted.csv";
    fieldHangle = LoadFieldMap(BfieldMapFileName, 10);

    const char* trakingOutDirectoryElectrons = "Outputs/trakingDirectory_electrons";
    const char* trakingOutDirectoryIons = "Outputs/trakingDirectory_ions";


    int initStepNums[nParticles];

    double xArr_e[nParticles];
    double yArr_e[nParticles];
    double zArr_e[nParticles];
    double vxArr_e[nParticles];
    double vyArr_e[nParticles];
    double vzArr_e[nParticles];

    double xArr_i[nParticles];
    double yArr_i[nParticles];
    double zArr_i[nParticles];
    double vxArr_i[nParticles];
    double vyArr_i[nParticles];
    double vzArr_i[nParticles];

    char headerLine[512];


    int fileRecordPeriod = 40000;
    int numOfSteps = 445000000; // the stepping properties coresponds to 89 miliseconds of simulations
    double stepTime = 2e-13;
    int ionStepTimeMultiplicity = 1000;

    const char* electronInitCoordinatesFileName = "geant4_out_electrons.csv";
    FILE* initFile_e = fopen(electronInitCoordinatesFileName, "r");

    const char* ionInitCoordinatesFileName = "geant4_out_ions.csv";
    FILE* initFile_i = fopen(ionInitCoordinatesFileName, "r");

    // skipping first line
    fgets(headerLine, sizeof(headerLine), initFile_i);
    fgets(headerLine, sizeof(headerLine), initFile_e);


    for(int i = 0; i < nParticles; ++i)
    {
        int initStepNum; // the init step numbers should be defined only in elecron file
                         // for ions in derived from time multiplicity and electron init step

        ////////////////////////      
        // Electrons
        ////////////////////////        

        double x_e,y_e,z_e,vx_e,vy_e,vz_e;

        fscanf(initFile_e, "%d,%lf,%lf,%lf,%lf,%lf,%lf", &initStepNum,&x_e,&y_e,&z_e,&vx_e,&vy_e,&vz_e);

        initStepNums[i] = initStepNum;

        xArr_e[i] = x_e;
        yArr_e[i] = y_e;
        zArr_e[i] = z_e;

        vxArr_e[i] = vx_e;
        vyArr_e[i] = vy_e;
        vzArr_e[i] = vz_e;

        ////////////////////////
        // Ions
        ////////////////////////        

        double x_i,y_i,z_i,vx_i,vy_i,vz_i;

        fscanf(initFile_i, "%lf,%lf,%lf,%lf,%lf,%lf", &x_i,&y_i,&z_i,&vx_i,&vy_i,&vz_i);


        xArr_i[i] = x_i;
        yArr_i[i] = y_i;
        zArr_i[i] = z_i;

        vxArr_i[i] = vx_i;
        vyArr_i[i] = vy_i;
        vzArr_i[i] = vz_i;
    }

    fclose(initFile_e);
    fclose(initFile_i);

    #pragma omp parallel for default(shared) schedule(dynamic)
    for(int i = 0; i < nParticles; ++i)
    {
        int initStep = initStepNums[i];
        int nTrackPars = 6;

        if(i % 100) printf("The tracking of particle number %d\n",i);

        // ////////////////////////
        // // Electrons
        // ////////////////////////

        // char trakingOutFileName_e[256];
        // sprintf(trakingOutFileName_e, "%s/track_%d.txt", trakingOutDirectoryElectrons, i);

        // double x0_e[] = {xArr_e[i], yArr_e[i], zArr_e[i], vxArr_e[i], vyArr_e[i], vzArr_e[i]};
        // double electronProperties[] = {-1, 9.1093837e-31}; //charge sign, mass in kg

        // int initStep_e = initStep;
        // double stepTime_e = stepTime;
        // int fileRecordPeriod_e = fileRecordPeriod;
        // int numOfSteps_e = numOfSteps;

        // FILE *outFile_e = fopen(trakingOutFileName_e, "w");
        // fprintf(outFile_e, "Init recording number %d\n", initStep_e/fileRecordPeriod_e); 
        // calcRungeKutta(&nTrackPars, x0_e, &numOfSteps_e, &initStep_e, &stepTime_e, outFile_e, &fileRecordPeriod_e, electronProperties);
        // fclose(outFile_e);

        ////////////////////////
        // Ions
        ////////////////////////

        char trakingOutFileName_i[256];
        sprintf(trakingOutFileName_i, "%s/track_%d.txt", trakingOutDirectoryIons, i);

        double x0_i[] = {xArr_i[i], yArr_i[i], zArr_i[i], vxArr_i[i], vyArr_i[i], vzArr_i[i]};
        interactionFuncOpts ionInteractProperties = { 1, 3.350967843e-26, fieldHangle, bunchFieldHandle}; //charge sign, mass in kg, field handles

        int initStep_i = initStep/ionStepTimeMultiplicity;
        double stepTime_i = stepTime*ionStepTimeMultiplicity;
        int numOfSteps_i = numOfSteps/ionStepTimeMultiplicity;

        int fileRecordPeriod_i = fileRecordPeriod/ionStepTimeMultiplicity;

        FILE *outFile_i = fopen(trakingOutFileName_i, "w");
        fprintf(outFile_i, "Init recording number %d\n", initStep_i/fileRecordPeriod_i); 
        // here initStep_e, because we need step in not multiplited time
        calcRungeKutta(CalculateInteractions, &nTrackPars, x0_i, &numOfSteps_i, &initStep_i, &stepTime_i, outFile_i, &fileRecordPeriod_i, &ionInteractProperties);
        fclose(outFile_i);        
    }

    deleteBfieldHandle(fieldHangle);
    deleteBfieldHandle2D(bunchFieldHandle);

    return 0;
}