import os
import moviepy.video.io.ImageSequenceClip
plotsDirectory='Outputs/ProtonfieldsPlots'
fps=64

image_files = [os.path.join(plotsDirectory, filename) for filename in\
               sorted(os.listdir(plotsDirectory), key=lambda x: int("".join([i for i in x if i.isdigit()])))]
clip = moviepy.video.io.ImageSequenceClip.ImageSequenceClip(image_files, fps=fps)
clip.write_videofile('Outputs/video_protonBeamFields.mp4')
