import pandas as pd
import matplotlib
import matplotlib.pyplot as plt

matplotlib.use('Agg') # this line is needed for threads safety in using matplotlib

import os
import io
import numpy as np

class singleTrakingResults:

    def __init__(self, dirname):
        self.trackFileNames = [os.path.join(dirname, filename) for filename in os.listdir(dirname) ]

        self.nParticles = len(self.trackFileNames)
        self.trkFiles = []
        self.skipRecords = []
                

    def __iter__(self):

        for file in self.trkFiles:
            if isinstance(file, io.TextIOWrapper):
                file.close()

        self.trkFiles = [open(filename, "r") for filename in self.trackFileNames]

        for file in self.trkFiles:
            line = file.readline()
            initStep = int(line.split()[-1])
            self.skipRecords.append(initStep)

        self.currentRecordNum = 0

        return self
    
    def __next__(self):

        self.currentRecordNum+=1

        files2process = np.array([iniTrcNum <= self.currentRecordNum for iniTrcNum in self.skipRecords], dtype=int)
        files2process = np.nonzero(files2process)

        result = np.empty((files2process[0].shape[0], 6))
        
        # no reason in further calculations is no files to process
        # but still needed to trturn empty array
        if files2process[0].shape[0] == 0: return result

        for i, fileNum in enumerate(files2process[0]):

            line = self.trkFiles[fileNum].readline()
            if line == "": result[i,:] = np.NAN
            else:
                strParamsList = line.split()
                for parN in range(6):
                    result[i,parN] = float(strParamsList[parN])


        if np.all(np.isnan(result[:,:])):            
            for file in self.trkFiles:
                file.close()

            raise StopIteration

        return result

# class collectiveTrackingResults:

#     def __init__(self, filename):
#         self.filename = filename
#         self.file = None
        

#     def __iter__(self):

#         if isinstance(self.file, io.TextIOWrapper):
#             self.file.close()

#         self.file = open(self.filename, "r");
    
#         return self

#     def __next__(self):

#         line = self.file.readline()

#         if line == "":
#             raise StopIteration

#         parameters = np.array([float(num) for num in line.split()])

#         (nParticles,) = parameters.shape
#         if nParticles % 6 != 0:
#             raise "The number of columns in file line in incorrect"
#         else:
#             nParticles = int(nParticles/6)

#         result = np.reshape(parameters, (nParticles, 6), "F")

#         return result   


zLim = 2
yLim = 0.03
stepTime = 2e-13
fileRecordPeriod = 20000

def createPltWrapper(func):
    func.fig, (func.ax1, func.ax2) = plt.subplots(1,2, figsize=(15, 7))
    return func

@createPltWrapper
def SavePlots(args):

    recordingNum, params_i = args

    print("Current recording is", recordingNum)

    x_i = params_i[:,0]
    y_i = params_i[:,1]
    z_i = params_i[:,2]

    SavePlots.ax1.scatter(x_i,y_i, marker=".", color="r")
    SavePlots.ax1.set_xlabel("x [m]")
    SavePlots.ax1.set_ylabel("y [m]")
    SavePlots.ax1.set_xlim([-yLim, yLim])
    SavePlots.ax1.set_ylim([-yLim, yLim])
    SavePlots.ax1.set_title("time = {:.4f} ns".format(fileRecordPeriod*stepTime*1e9*(recordingNum+1)))

    SavePlots.ax2.scatter(z_i,y_i, marker=".", color="r")
    SavePlots.ax2.set_xlabel("z [m]")
    SavePlots.ax2.set_ylabel("y [m]")
    SavePlots.ax2.set_xlim([-zLim, zLim])
    SavePlots.ax2.set_ylim([-yLim, yLim])

    plotsDirectory = "Outputs/plots"
    SavePlots.fig.savefig(plotsDirectory + f"/recording_{recordingNum+1}.png")
    
    SavePlots.ax1.clear()
    SavePlots.ax2.clear()

    del x_i
    del y_i
    del z_i


import multiprocessing

with multiprocessing.Pool() as pool_obj:
    pool_obj.map(SavePlots, enumerate(singleTrakingResults("Outputs/trakingDirectory_ions")))