#include "InteractionProcessing.h"
#include "BeamsFields.h"

#include <math.h>
#include <stdlib.h>


void CalculateInteractions(const int* restrict n, const double *restrict x, double *restrict f, 
                           const double* restrict cuttentTime, void* restrict funcOptsVoid, 
                           int* restrict ierr)
{
    *ierr = 0;
    interactionFuncOpts* funcOpts = (interactionFuncOpts*)funcOptsVoid;

    const double e = 1.60217663E-19; //coulomb

    const double *xCoor = x;
    const double *yCoor = x + 1;
    const double *zCoor = x + 2;

    double r = sqrt(xCoor[0]*xCoor[0]+yCoor[0]*yCoor[0]);
    if(r > 0.03 || fabs(zCoor[0]) > 1.7)
    {
        *ierr = 1; //the flag means we are outside deevice
        return;
    }

    const double *xDot = x + 3;
    const double *yDot = x + 4;
    const double *zDot = x + 5;


    double* fx = f;
    double* fy = f + 1;
    double* fz = f + 2;

    double* fxDot = f + 3;
    double* fyDot = f + 4;
    double* fzDot = f + 5;

    // interaction with magnetic field and fx, fy, fz
    *fx = *xDot;
    *fy = *yDot;
    *fz = *zDot;

    double eBx=0;
    double eBy=0;
    double eBz=0;
    
    double eEx=0;
    double eEy=0;
    double eEz=0;

    double tmpFex, tmpFey, tmpFez, tmpFbx, tmpFby, tmpFbz;

    BFieldHandle* fieldHangle = funcOpts->guidingBfield;
    GetBfield(fieldHangle, xCoor, yCoor, zCoor, &tmpFbx, &tmpFby, &tmpFbz, ierr);

    if (*ierr > 0)
    {
        return; //no reason to further calculate if the particle is outside the field map
    }

    eBx+=tmpFbx;
    eBy+=tmpFby;
    eBz+=tmpFbz;

    CalcElectrBeamFields(cuttentTime, xCoor, yCoor, zCoor, &tmpFbx, &tmpFby, &tmpFbz, &tmpFex, &tmpFey, &tmpFez);
    eBx+=tmpFbx;
    eBy+=tmpFby;
    eBz+=tmpFbz;
    eEx+=tmpFex;
    eEy+=tmpFey;
    eEz+=tmpFez;

    BFieldHandle2D* bunchFieldHandle = funcOpts->bunchEfield;
    CalcProtBeamFields(cuttentTime, xCoor, yCoor, zCoor, &tmpFbx, &tmpFby, &tmpFbz, &tmpFex, &tmpFey, &tmpFez, ierr, bunchFieldHandle);
    eBx+=tmpFbx;
    eBy+=tmpFby;
    eBz+=tmpFbz;
    eEx+=tmpFex;
    eEy+=tmpFey;
    eEz+=tmpFez;

    eBx*=e;
    eBy*=e;
    eBz*=e;

    eEx*=e;
    eEy*=e;
    eEz*=e;


    double pChar = funcOpts->charge;
    double particleMasses = funcOpts->mass;

    *fxDot = pChar*(yDot[0]*eBz - zDot[0]*eBy + eEx)/particleMasses;
    *fyDot = pChar*(zDot[0]*eBx - xDot[0]*eBz + eEy)/particleMasses;
    *fzDot = pChar*(xDot[0]*eBy - yDot[0]*eBx + eEz)/particleMasses;

}