#include <stdio.h>
#include "BFieldHandle.h"


int main(int argc, char** argv)
{

    BFieldHandle2D* bunchFieldHandle = LoadFieldMap2D("Proton bunch field map.csv", 0.03/499, 2*83928.375/999);

    double zLim = 83928.375;
    double rLim = 0.03;

    int zDivs = 6000;
    int rDivs = 3000;

    FILE* file = fopen("FieldMapTest.txt", "w");

    for(int ir = 0; ir < rDivs; ++ir)
    {
        double r = rLim/(rDivs-1)*ir;

        for(int iz = 0; iz < zDivs; ++iz)
        {
            double z = 2*zLim/(zDivs-1)*iz - zLim;

            int ierr = 0;

            double Br, Bz;

            GetBfield2D(bunchFieldHandle, &r, &z, &Br, &Bz, &ierr);

            if(ierr > 0) Br = Bz = 0;

            fprintf(file, "%e %e %e %e \n", r, z, Br, Bz);

        }
    }

    fclose(file);
    deleteBfieldHandle2D(bunchFieldHandle);

    return 0;
}