#ifndef __RungeCutta_h__
#define __RungeCutta_h__

#include "stdio.h"

typedef void (*diffEqFunc)(const int* restrict n, const double *restrict x, double *restrict f, 
       const double* restrict cuttentTime, void* restrict funcOpts, 
       int* restrict ierr);

void calcRungeKutta(diffEqFunc CalculateInteractions, const int* restrict nPars, double* restrict x0, 
                    const int* restrict numOfSteps, const int* restrict initStep, const double* restrict stepTime, 
                    FILE * restrict outFile, int *restrict fileRecordPeriod, void* restrict funcOptions);

#endif //__RungeCutta_h__