#include "BFieldHandle.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

BFieldHandle* LoadFieldMap(const char* filename, double mapStep)
{
    

    FILE *file = fopen(filename,"r");

    char headerLine[128];
    fgets(headerLine, sizeof(headerLine), file);

    double minX, maxX;
    double minY, maxY;
    double minZ, maxZ;

    double x, y, z, Bx, By, Bz;

    fscanf(file, "%lf,%lf,%lf,%lf,%lf,%lf", 
            &minX,&minY,&minZ,&Bx,&By,&Bz);            

    maxX = minX;//initialization of initial values
    maxY = minY;//final values will be determined after the file scan
    maxZ = minZ;

    while(fscanf(file, "%lf,%lf,%lf,%lf,%lf,%lf",&x,&y,&z,&Bx,&By,&Bz) != EOF)
    {
        if (x < minX) minX = x;
        if (x < minY) minY = y;
        if (z < minZ) minZ = z;
        if (x > maxX) maxX = x;
        if (y > maxY) maxY = y;
        if (z > maxZ) maxZ = z;
    }

    BFieldHandle* result = malloc(sizeof(BFieldHandle));

    result->mapStep = mapStep;

    result->xMinPoint = minX;
    result->yMinPoint = minY;
    result->zMinPoint = minZ;

    result->xMaxPoint = maxX;
    result->yMaxPoint = maxY;
    result->zMaxPoint = maxZ;

    if(maxX == minX || maxY == minY || maxZ == minZ)
    {
        printf("ERROR: The field map is not correct!");
        exit(1);
    }

    result->xIdxSize = round((maxX-minX) / mapStep) + 1;
    result->yIdxSize = round((maxY-minY) / mapStep) + 1;
    result->zIdxSize = round((maxZ-minZ) / mapStep) + 1;


    result->BxMap = malloc(result->xIdxSize*sizeof(double**));
    result->ByMap = malloc(result->xIdxSize*sizeof(double**));
    result->BzMap = malloc(result->xIdxSize*sizeof(double**));

    for(int xIdx = 0; xIdx < result->xIdxSize; ++xIdx)
    {
        result->BxMap[xIdx] = malloc(result->yIdxSize*sizeof(double*));
        result->BzMap[xIdx] = malloc(result->yIdxSize*sizeof(double*));
        result->ByMap[xIdx] = malloc(result->yIdxSize*sizeof(double*));

        for(int yIdx = 0; yIdx < result->yIdxSize; ++yIdx)
        {
            result->BxMap[xIdx][yIdx] = malloc(result->zIdxSize*sizeof(double));
            result->BzMap[xIdx][yIdx] = malloc(result->zIdxSize*sizeof(double));
            result->ByMap[xIdx][yIdx] = malloc(result->zIdxSize*sizeof(double));
        }
    }


    rewind(file);//return to the beginning of the file

    fgets(headerLine, sizeof(headerLine), file);


    while(fscanf(file, "%lf,%lf,%lf,%lf,%lf,%lf",&x,&y,&z,&Bx,&By,&Bz) != EOF)
    {
        int xIdx = round((x - result->xMinPoint)/mapStep);
        int yIdx = round((y - result->yMinPoint)/mapStep);
        int zIdx = round((z - result->zMinPoint)/mapStep);

        if(xIdx >= result->xIdxSize || yIdx >= result->yIdxSize ||
           zIdx >= result->zIdxSize)
        {
            printf("Warning, index error in a field map\n");
            printf("xIdx = %d, xIdxSize - %d, yIdx = %d, yIdxSize = %d, zIdx = %d, zIdxSize = %d\n",
                    xIdx, result->xIdxSize, yIdx, result->yIdxSize, zIdx, result->zIdxSize);
        }

        result->BxMap[xIdx][yIdx][zIdx] = Bx;
        result->ByMap[xIdx][yIdx][zIdx] = By;
        result->BzMap[xIdx][yIdx][zIdx] = Bz;
    }


    fclose(file);

    return result;
}

void GetBfield(const BFieldHandle* aHandle, const double* restrict xM, const double* restrict yM, const double* restrict zM,
                 double* restrict BxOut, double * restrict ByOut, double * restrict BzOut,  int* restrict ierr)
{
    // scale to millimetres, as the field map is in millimetres
    double x = *xM * 1000;
    double y = *yM * 1000;
    double z = *zM * 1000;

    double*** BxM = aHandle->BxMap;
    double*** ByM = aHandle->ByMap;
    double*** BzM = aHandle->BzMap;

    double xMinPoint = aHandle->xMinPoint;
    double yMinPoint = aHandle->yMinPoint;
    double zMinPoint = aHandle->zMinPoint;    

    double ms = aHandle->mapStep;

    int xIdx = round((x - xMinPoint)/ms);
    int yIdx = round((y - yMinPoint)/ms);
    int zIdx = round((z - zMinPoint)/ms);

    // prevention from index out-of-array error
    // the equation may be wrongly fulfilled due to round in 
    // indexes calculation
    if(xIdx == aHandle->xIdxSize-1) xIdx--;
    if(yIdx == aHandle->yIdxSize-1) yIdx--;
    if(zIdx == aHandle->zIdxSize-1) zIdx--;

    // pervent from going outside the array range 
    // in case point in on the map border
    if (x == aHandle->xMaxPoint) xIdx--;
    if (y == aHandle->yMaxPoint) yIdx--;
    if (z == aHandle->zMaxPoint) zIdx--;

    // return corresponding ierr flag if requested coordinates are outside the map rang
    if (x > aHandle->xMaxPoint || x < aHandle->xMinPoint ||
        y > aHandle->yMaxPoint || y < aHandle->yMinPoint ||
        z > aHandle->zMaxPoint || z < aHandle->zMinPoint  )
    {
        *ierr = 1; //the flag means we are outside the field map ranges
        return;
    }
    else
    {
        *ierr = 0;
    }


    if(xIdx >= aHandle->xIdxSize-1 || yIdx >= aHandle->yIdxSize-1 ||
       zIdx >= aHandle->zIdxSize-1)
    {
        printf("Warning, index error in a field map\n");
        printf("xIdx = %d, xIdxSize - %d, yIdx = %d, yIdxSize = %d, zIdx = %d, zIdxSize = %d\n",
                xIdx, aHandle->xIdxSize, yIdx, aHandle->yIdxSize, zIdx, aHandle->zIdxSize);
    }


    // we will work only from residual from the map in furthers
    x -= ms*xIdx + xMinPoint;
    y -= ms*yIdx + yMinPoint;
    z -= ms*zIdx + zMinPoint;

    ////////////////////
    // Bx
    ////////////////////

    double Bx[] = {BxM[xIdx+1][yIdx+1][zIdx+1],BxM[xIdx][yIdx+1][zIdx+1],
                   BxM[xIdx+1][yIdx][zIdx+1],BxM[xIdx+1][yIdx+1][zIdx],
                   BxM[xIdx+1][yIdx][zIdx],BxM[xIdx][yIdx+1][zIdx],
                   BxM[xIdx][yIdx][zIdx+1],BxM[xIdx][yIdx][zIdx]};

    double A = (Bx[0]-Bx[1]-Bx[2]-Bx[3]+Bx[4]+Bx[5]+Bx[6]-Bx[7])/(ms*ms*ms);
    double B = (Bx[1]-Bx[5]-Bx[6]+Bx[7])/(ms*ms);
    double C = (Bx[2]-Bx[4]-Bx[6]+Bx[7])/(ms*ms);
    double D = (Bx[3]-Bx[4]-Bx[5]+Bx[7])/(ms*ms);
    double E = (Bx[4]-Bx[7])/ms;
    double F = (Bx[5]-Bx[7])/ms;
    double G = (Bx[6]-Bx[7])/ms;
    double H = Bx[7];
    
    *BxOut = A*x*y*z + B*y*z + C*x*z + D*x*y + E*x + F*y + G*z + H;

    ////////////////////
    // By
    ////////////////////

    double By[] = {ByM[xIdx+1][yIdx+1][zIdx+1],ByM[xIdx][yIdx+1][zIdx+1],
                   ByM[xIdx+1][yIdx][zIdx+1],ByM[xIdx+1][yIdx+1][zIdx],
                   ByM[xIdx+1][yIdx][zIdx],ByM[xIdx][yIdx+1][zIdx],
                   ByM[xIdx][yIdx][zIdx+1],ByM[xIdx][yIdx][zIdx]};

    A = (By[0]-By[1]-By[2]-By[3]+By[4]+By[5]+By[6]-By[7])/(ms*ms*ms);
    B = (By[1]-By[5]-By[6]+By[7])/(ms*ms);
    C = (By[2]-By[4]-By[6]+By[7])/(ms*ms);
    D = (By[3]-By[4]-By[5]+By[7])/(ms*ms);
    E = (By[4]-By[7])/ms;
    F = (By[5]-By[7])/ms;
    G = (By[6]-By[7])/ms;
    H = By[7];    

    *ByOut = A*x*y*z + B*y*z + C*x*z + D*x*y + E*x + F*y + G*z + H;

    ////////////////////
    // By
    ////////////////////

    double Bz[] = {BzM[xIdx+1][yIdx+1][zIdx+1],BzM[xIdx][yIdx+1][zIdx+1],
                   BzM[xIdx+1][yIdx][zIdx+1],BzM[xIdx+1][yIdx+1][zIdx],
                   BzM[xIdx+1][yIdx][zIdx],BzM[xIdx][yIdx+1][zIdx],
                   BzM[xIdx][yIdx][zIdx+1],BzM[xIdx][yIdx][zIdx]};    

    A = (Bz[0]-Bz[1]-Bz[2]-Bz[3]+Bz[4]+Bz[5]+Bz[6]-Bz[7])/(ms*ms*ms);
    B = (Bz[1]-Bz[5]-Bz[6]+Bz[7])/(ms*ms);
    C = (Bz[2]-Bz[4]-Bz[6]+Bz[7])/(ms*ms);
    D = (Bz[3]-Bz[4]-Bz[5]+Bz[7])/(ms*ms);
    E = (Bz[4]-Bz[7])/ms;
    F = (Bz[5]-Bz[7])/ms;
    G = (Bz[6]-Bz[7])/ms;
    H = Bz[7];

    *BzOut = A*x*y*z + B*y*z + C*x*z + D*x*y + E*x + F*y + G*z + H;

}


void deleteBfieldHandle(BFieldHandle* aHandle)
{
    for(int xIdx = 0; xIdx < aHandle->xIdxSize; ++xIdx)
    {
        for(int yIdx = 0; yIdx < aHandle->yIdxSize; ++yIdx)
        {
            free(aHandle->BxMap[xIdx][yIdx]);
            free(aHandle->BzMap[xIdx][yIdx]);
            free(aHandle->ByMap[xIdx][yIdx]);
        }
        free(aHandle->BxMap[xIdx]);
        free(aHandle->BzMap[xIdx]);
        free(aHandle->ByMap[xIdx]);
    }

    free(aHandle->BxMap);
    free(aHandle->ByMap);
    free(aHandle->BzMap);  

    free(aHandle);

}


//====================================================================//
//                        2D FIELD MAP


BFieldHandle2D* LoadFieldMap2D(const char* filename, double dr, double dz)
{
    
    FILE *file = fopen(filename,"r");

    char headerLine[128];
    fgets(headerLine, sizeof(headerLine), file);

    double minR, maxR;
    double minZ, maxZ;

    double r, z, Br, Bz;

    fscanf(file, "%lf,%lf,%lf,%lf", 
            &minR,&minZ,&Br,&Bz);            

    maxR = minR;//initialization of initial values
    maxZ = minZ;//final values will be determined after the file scan

    while(fscanf(file, "%lf,%lf,%lf,%lf",&r,&z,&Br,&Bz) != EOF)
    {
        if (r < minR) minR = r;
        if (z < minZ) minZ = z;
        if (r > maxR) maxR = r;
        if (z > maxZ) maxZ = z;
    }

    BFieldHandle2D* result = malloc(sizeof(BFieldHandle));

    result->dr = dr;
    result->dz = dz;

    result->rMinPoint = minR;
    result->zMinPoint = minZ;

    result->rMaxPoint = maxR;
    result->zMaxPoint = maxZ;

    if(maxR == minR || maxZ == minZ)
    {
        printf("ERROR: The field map is not correct!");
        exit(1);
    }

    result->rIdxSize = round((maxR-minR) / dr) + 1;
    result->zIdxSize = round((maxZ-minZ) / dz) + 1;


    result->BrMap = malloc(result->rIdxSize*sizeof(double*));
    result->BzMap = malloc(result->rIdxSize*sizeof(double*));

    for(int rIdx = 0; rIdx < result->rIdxSize; ++rIdx)
    {   
        result->BrMap[rIdx] = malloc(result->zIdxSize*sizeof(double));
        result->BzMap[rIdx] = malloc(result->zIdxSize*sizeof(double));
    }


    rewind(file);//return to the beginning of the file

    fgets(headerLine, sizeof(headerLine), file);


    while(fscanf(file, "%lf,%lf,%lf,%lf",&r,&z,&Br,&Bz) != EOF)
    {
        int rIdx = round((r - result->rMinPoint)/dr);
        int zIdx = round((z - result->zMinPoint)/dz);

        if(rIdx >= result->rIdxSize || zIdx >= result->zIdxSize)
        {
            printf("Warning, index error in 2D field map\n");
            printf("rIdx = %d, rIdxSize - %d, zIdx = %d, zIdxSize = %d\n",
                    rIdx, result->rIdxSize, zIdx, result->zIdxSize);
        }

        result->BrMap[rIdx][zIdx] = Br;
        result->BzMap[rIdx][zIdx] = Bz;
    }


    fclose(file);

    return result;
}


void deleteBfieldHandle2D(BFieldHandle2D* aHandle)
{
    int idr = aHandle->rIdxSize;

    for(int i = 0; i < idr; ++i)
    {
        free(aHandle->BrMap[i]);
        free(aHandle->BzMap[i]);
    }

    free(aHandle->BrMap);
    free(aHandle->BzMap);

    free(aHandle);

}

void GetBfield2D(const BFieldHandle2D* aHandle, const double* restrict r, const double* restrict z,
                 double* restrict eBr, double * restrict eBz, int* restrict ierr)
{
  
    if (z[0] <= aHandle->zMinPoint || z[0] >= aHandle->zMaxPoint 
        || r[0] <= aHandle->rMinPoint || r[0] >= aHandle->rMaxPoint )
    {
        *eBr = 0;
        *eBz = 0;

        return;
    }

    int i = round((r[0] - aHandle->rMinPoint) / aHandle->dr);
    int j = round((z[0] - aHandle->zMinPoint) / aHandle->dz);

    // prevention from index out-of-array error
    // the equation may be wrongly fulfilled due to round in 
    // indexes calculation
    if(i == aHandle->rIdxSize-1) i--;
    if(j == aHandle->zIdxSize-1) j--;


    if(i >= aHandle->rIdxSize-1 || j >= aHandle->zIdxSize-1)
    {
        printf("Warning, index error in 2D field map\n");
        printf("i = %d, iMax - %d, j = %d, jMax = %d\n",i, aHandle->rIdxSize, j, aHandle->zIdxSize);
    }

    double z_c = z[0] - (j*aHandle->dz + aHandle->zMinPoint);
    double rho_c = r[0] - (i*aHandle->dr + aHandle->rMinPoint);

    double** Erho_beam_map = aHandle->BrMap;
    double** Ez_beam_map = aHandle->BzMap;

    double dz = aHandle->dz;
    double dr = aHandle->dr;


    double A_Erho = (Erho_beam_map[i+1][j+1] + Erho_beam_map[i][j] - Erho_beam_map[i][j+1] - Erho_beam_map[i+1][j])/(dz*dr);
    double B_Erho = (Erho_beam_map[i][j+1] - Erho_beam_map[i][j])/dz;
    double C_Erho = (Erho_beam_map[i+1][j] - Erho_beam_map[i][j])/dr;
    double D_Erho = Erho_beam_map[i][j];


    double A_Ez = (Ez_beam_map[i+1][j+1] + Ez_beam_map[i][j] - Ez_beam_map[i][j+1] - Ez_beam_map[i+1][j])/(dz*dr);
    double B_Ez = (Ez_beam_map[i][j+1] - Ez_beam_map[i][j])/dz;
    double C_Ez = (Ez_beam_map[i+1][j] - Ez_beam_map[i][j])/dr;
    double D_Ez = Ez_beam_map[i][j];

    *eBr = A_Erho*z_c*rho_c + B_Erho*z_c + C_Erho*rho_c + D_Erho;
    *eBz = A_Ez*z_c*rho_c + B_Ez*z_c + C_Ez*rho_c + D_Ez;

}
