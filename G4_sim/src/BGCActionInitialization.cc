#include "BGCActionInitialization.hh"

#include "BGCRunAction.hh"
#include "BGCPrimaryGeneratorAction.hh"
#include "BGCTrackingAction.hh"

void BGCActionInitialization::BuildForMaster() const
{
    SetUserAction(new BGCRunAction);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BGCActionInitialization::Build() const
{
    SetUserAction(new BGCRunAction);
    SetUserAction(new BGCPrimaryGeneratorAction);
    SetUserAction(new BGCTrackingAction);
}