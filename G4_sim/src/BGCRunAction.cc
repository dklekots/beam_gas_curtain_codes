#include "BGCRunAction.hh"

#include "G4AnalysisManager.hh"

#include "G4Run.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BGCRunAction::BGCRunAction(): G4UserRunAction()
{
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  analysisManager->SetVerboseLevel(1);
  analysisManager->SetNtupleMerging(true);

  // Creating ntuples
  //
  // ntuple id = 0
  analysisManager->CreateNtuple("Secondaries", "Secondary electrons properties");
  analysisManager->CreateNtupleIColumn("EventID");      // column id = 0 
  analysisManager->CreateNtupleDColumn("Px");           // column id = 1 
  analysisManager->CreateNtupleDColumn("Py");           // column id = 2
  analysisManager->CreateNtupleDColumn("Pz");           // column id = 3 
  analysisManager->CreateNtupleDColumn("Ekin");         // column id = 4
  analysisManager->CreateNtupleDColumn("P");            // column id = 5
  analysisManager->CreateNtupleDColumn("x");            // column id = 6
  analysisManager->CreateNtupleDColumn("y");            // column id = 7
  analysisManager->CreateNtupleDColumn("z");            // column id = 8
  analysisManager->CreateNtupleIColumn("trkID");        // column id = 9

  analysisManager->FinishNtuple();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BGCRunAction::~BGCRunAction()
{
  delete G4AnalysisManager::Instance();  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BGCRunAction::BeginOfRunAction(const G4Run* /*run*/)
{ 
  // Open an output file
  //
  G4String fileName = "Secondaries.root";
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  analysisManager->OpenFile(fileName);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BGCRunAction::EndOfRunAction(const G4Run* /*run*/)
{  
  // save histograms 
  //
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  analysisManager->Write();
  analysisManager->CloseFile();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
