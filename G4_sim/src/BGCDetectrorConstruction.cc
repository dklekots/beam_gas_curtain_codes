#include "BGCDetectrorConstruction.hh"

#include "G4SystemOfUnits.hh"
#include "G4LogicalVolume.hh"

#include "G4Element.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"

#include "G4UnionSolid.hh"
#include "G4Tubs.hh"
#include "G4Box.hh"

BGCDetectorConstruction::BGCDetectorConstruction()
{
    G4cout << "The detector construncion class constructor" << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


BGCDetectorConstruction::~BGCDetectorConstruction()
{
    G4cout << "The detector construncion class destructor" << G4endl;
}

G4VPhysicalVolume* BGCDetectorConstruction::Construct()
{

    //////////////////////////
    // materials
    //////////////////////////

    // LHC vacuum
    G4Element* elN = new G4Element("Nitrogen", "N", 7.0, 14.01*g/mole);
    G4Element* elO = new G4Element("Oxygen", "O", 8.0, 16.00*g/mole);
    G4Material* vacuum = new G4Material("Vacuum", 1.164e-32*g/cm3, 2, kStateGas);
    vacuum->AddElement(elN, 70.0*perCent);
    vacuum->AddElement(elO, 30.0*perCent); 


    // Neon in curtain
    G4double particlesDensity = 8e16/m3;
    G4double molarMass = 20.1797*g/mole;
    G4double avogadroNumber = 6.02E23/mole;
    G4double massDensity = molarMass/avogadroNumber*particlesDensity;
    G4Material* neon = new G4Material("Neon", 10.0, molarMass, massDensity, kStateGas);




    //////////////////////////
    // world volume
    //////////////////////////    

    G4Tubs* lhcBeamPipeS = new G4Tubs("lhcBeamPipeS", 0, 50*mm, 139.3*mm, 0, 360*degree);

    G4Tubs* gasJetPipeS = new G4Tubs("gasJetPipeS", 0, 30*mm, 177.295*mm, 0, 360*degree);

    G4Tubs* pompPipeS = new G4Tubs("pompPipeS", 0, 50*mm, 144.45*mm, 0, 360*degree);

    G4Tubs* opticPipeS = new G4Tubs("opticPipeS", 0, 20.25*mm, 61.845*mm, 0, 360*degree);

    G4RotationMatrix* gasJetPipeRot = new G4RotationMatrix();
    gasJetPipeRot->rotateY(90*degree);
    G4ThreeVector gasJetPipeTr(47.905,0,0);
    G4VSolid* beamAndJetPipesS = new G4UnionSolid("beamAndJetPipesS", lhcBeamPipeS,
                                                  gasJetPipeS, gasJetPipeRot, gasJetPipeTr);


    G4RotationMatrix* pompPipeRot = new G4RotationMatrix();
    pompPipeRot->rotateX(90*degree);  
    G4ThreeVector pompPipeTr(0,-144.45,0);
    G4VSolid* beamJetPompPipesS = new G4UnionSolid("beamJetPompPipesS", beamAndJetPipesS,
                                                   pompPipeS, pompPipeRot, pompPipeTr);


    G4RotationMatrix* opticPipeRot = new G4RotationMatrix();
    opticPipeRot->rotateX(90*degree);  
    G4ThreeVector opticPipeTr(0,30.9225,0);
    G4VSolid* worldS = new G4UnionSolid("beamJetPompPipesS", beamJetPompPipesS,
                                                   opticPipeS, opticPipeRot, opticPipeTr);


    G4LogicalVolume* worldLV = new G4LogicalVolume(worldS, vacuum, "World");

    G4PVPlacement *worldPV = new G4PVPlacement(nullptr,    // no rotation
                                G4ThreeVector(),           // at (0,0,0)
                                worldLV,                   // its logical volume
                                "World",                   // its name
                                nullptr,                   // its mother  volume
                                false,                     // no boolean operations
                                0,                         // copy number
                                true);                     // check overlaps


    //////////////////////////
    // gas curtain
    //////////////////////////      

    G4Box *gasCurtainS = new G4Box("gasCurtainS", 11*mm, 1.1*mm, 177.295*mm);

    G4RotationMatrix* gasCurtainRot = new G4RotationMatrix();
    gasCurtainRot->rotateY(90*degree);
    gasCurtainRot->rotateZ(45*degree);
    G4ThreeVector gasCurtainTr(47.905,0,0);

    G4LogicalVolume *gasCurtainLV = new G4LogicalVolume(gasCurtainS, neon, "gasCurtainLV");

    G4VPhysicalVolume* gasCurtainPV = new G4PVPlacement(gasCurtainRot,
                                                        gasCurtainTr,
                                                        "gasCurtain",
                                                        gasCurtainLV,
                                                        worldPV, 
                                                        false,
                                                        0,
                                                        true);


    return worldPV;

}
