#include "BGCTrackingAction.hh"

#include "G4SystemOfUnits.hh"
#include "G4AnalysisManager.hh"
#include "G4RunManager.hh"
#include "G4Event.hh"


BGCTrackingAction::BGCTrackingAction()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BGCTrackingAction::~BGCTrackingAction()
{
    
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BGCTrackingAction::PreUserTrackingAction(const G4Track* aTrack)
{
    if(aTrack->GetParentID() == 1) // if it is the track is delta from primary track
    {
        // Dumpling secondary electron properties into the analysis file
        G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
        analysisManager->FillNtupleIColumn(0, 
                            G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID());
        analysisManager->FillNtupleDColumn(1, aTrack->GetDynamicParticle()->GetMomentum().getX()/keV); //FIXME: For unknown for me reasons Ekin < P in the output file
        analysisManager->FillNtupleDColumn(2, aTrack->GetDynamicParticle()->GetMomentum().getY()/keV);
        analysisManager->FillNtupleDColumn(3, aTrack->GetDynamicParticle()->GetMomentum().getZ()/keV);
        analysisManager->FillNtupleDColumn(4, aTrack->GetKineticEnergy()/keV);
        analysisManager->FillNtupleDColumn(5, aTrack->GetMomentum().mag()/keV);
        analysisManager->FillNtupleDColumn(6, aTrack->GetPosition().getX()/mm);
        analysisManager->FillNtupleDColumn(7, aTrack->GetPosition().getY()/mm);
        analysisManager->FillNtupleDColumn(8, aTrack->GetPosition().getZ()/mm);
        analysisManager->FillNtupleIColumn(9, aTrack->GetTrackID());
        analysisManager->AddNtupleRow();
    }

    //FIXME: The biasing correctly works only if there is one 
    //interaction (one secondary particle) per event. Manage 
    //to process only correct events.

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BGCTrackingAction::PostUserTrackingAction(const G4Track*)
{
    
}