#include "BGCPrimaryGeneratorAction.hh"

#include "G4ParticleDefinition.hh"
#include "G4ParticleTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4ThreeVector.hh"
#include "G4PrimaryParticle.hh"
#include "G4PrimaryVertex.hh"
#include "G4Event.hh"

#include "Randomize.hh"
#include "G4INCLRandom.hh"

BGCPrimaryGeneratorAction::BGCPrimaryGeneratorAction()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BGCPrimaryGeneratorAction::~BGCPrimaryGeneratorAction()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BGCPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{

    /////////////////////////////////////////////////////////////////
    // Generate particle from electron hollow beam
    /////////////////////////////////////////////////////////////////


    // G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();

    // G4ParticleDefinition* particleDefinition = particleTable->FindParticle("e-");

    // G4PrimaryParticle* primaryParticle = new G4PrimaryParticle(particleDefinition);

    // G4double innerRadius = 2.5*mm;
    // G4double outerRadius = 5*mm;


    // G4double vertexX = 0;
    // G4double vertexY = 0;

    // while (vertexX*vertexX + vertexY*vertexY < innerRadius*innerRadius ||
    //        vertexX*vertexX + vertexY*vertexY > outerRadius*outerRadius)
    // {
    //     vertexX = G4UniformRand()*2*outerRadius - outerRadius;
    //     vertexY = G4UniformRand()*2*outerRadius - outerRadius;
    // }

    // G4double vertexZ = 139.3*mm;

    // G4ThreeVector position(vertexX, vertexY, vertexZ);
    // G4ThreeVector direction(0, 0, -1);
    // G4double energy = 10*keV;


    // primaryParticle->SetKineticEnergy(energy);
    // primaryParticle->SetMomentumDirection(direction);

    // G4PrimaryVertex* vertex = new G4PrimaryVertex(position, 0);
    // vertex->SetPrimary(primaryParticle);

    // anEvent->AddPrimaryVertex(vertex);


    /////////////////////////////////////////////////////////////////
    // Generate particle from proton LHC beam
    /////////////////////////////////////////////////////////////////

    G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();

    G4ParticleDefinition* particleDefinition = particleTable->FindParticle("proton");

    G4PrimaryParticle* primaryParticle = new G4PrimaryParticle(particleDefinition);


    G4double sigma = 306*um;

    G4double vertexX = G4INCL::Random::gauss()*sigma;
    G4double vertexY = G4INCL::Random::gauss()*sigma;
    G4double vertexZ = -139.3*mm;

    G4ThreeVector position(vertexX, vertexY, vertexZ);
    G4ThreeVector direction(0, 0, 1);
    G4double energy = 7*TeV;

    primaryParticle->SetKineticEnergy(energy);
    primaryParticle->SetMomentumDirection(direction);

    G4PrimaryVertex* vertex = new G4PrimaryVertex(position, 0);
    vertex->SetPrimary(primaryParticle);

    anEvent->AddPrimaryVertex(vertex);

}