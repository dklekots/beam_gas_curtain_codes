#include "BGCPhysicsList.hh"

#include "G4Proton.hh"
#include "G4Electron.hh"
#include "G4Positron.hh"
#include "G4Gamma.hh"

#include "G4PhysicsListHelper.hh"
#include "G4SystemOfUnits.hh"

#include "G4eIonisation.hh"
#include "G4LivermoreIonisationModel.hh"
#include "G4MollerBhabhaModel.hh"
#include "G4PAIModel.hh"

#include "G4hIonisation.hh"
#include "G4BraggModel.hh"
#include "G4BetheBlochModel.hh"
#include "G4UniversalFluctuation.hh"

#include <fstream>
#include <math.h>

BGCPhysicsList::BGCPhysicsList()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
BGCPhysicsList::~BGCPhysicsList()
{

}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BGCPhysicsList::ConstructParticle()
{
    G4Electron::ElectronDefinition();
    G4Proton::ProtonDefinition();

    G4Positron::PositronDefinition();
    G4Gamma::GammaDefinition();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BGCPhysicsList::ConstructProcess()
{

    AddTransportation();

    G4PhysicsListHelper *ph = 
            G4PhysicsListHelper::GetPhysicsListHelper();

    G4eIonisation* eIonisation = new G4eIonisation;
    G4LivermoreIonisationModel* eIonLiverm = 
                    new G4LivermoreIonisationModel;
    G4MollerBhabhaModel* eIonMollBha = 
                    new G4MollerBhabhaModel;
    eIonLiverm->SetHighEnergyLimit(1*MeV);
    eIonMollBha->SetLowEnergyLimit(1*MeV);
    eIonisation->AddEmModel(0, eIonLiverm, new G4PAIModel);
    eIonisation->AddEmModel(0, eIonMollBha, new G4PAIModel);
    ph->RegisterProcess(eIonisation, G4Electron::ElectronDefinition());

    eIonisation->SetCrossSectionBiasingFactor(100000); // do not forget about thease biasing
                                                       // DODO: find the optimal biasing
                                                       // taking into account that there should be single interaction in volume.


    G4hIonisation* hIonisation = new G4hIonisation;
    G4BraggModel* braggModel = new G4BraggModel;
    G4BetheBlochModel* betBlochModel = new G4BetheBlochModel;
    braggModel->SetHighEnergyLimit(2*MeV);
    betBlochModel->SetLowEnergyLimit(2*MeV);
    hIonisation->AddEmModel(0, braggModel, new G4UniversalFluctuation);
    hIonisation->AddEmModel(0, betBlochModel, new G4UniversalFluctuation);
    ph->RegisterProcess(hIonisation, G4Proton::ProtonDefinition());

    hIonisation->SetCrossSectionBiasingFactor(100000); // do not forget about thease biasing
                                                       // DODO: find the optimal biasing
                                                       // taking into account that there should be single interaction in volume.

    /////////////////////////////////////////////////////
    // fixme
    
    double nPoint = 100;
    double dx = pow(7*TeV/(2*MeV), 1.0/nPoint);

    std::ofstream ofFile("betBlochCrossection.txt");

    for(int i = 0; i <= nPoint; ++i)
    {
        double x = 2*MeV*pow(dx,i);
        double y = betBlochModel->ComputeCrossSectionPerAtom(G4Proton::ProtonDefinition(), x, 10, 20.1797*g/mole, 137.3*eV, 7*TeV)/barn;

        ofFile << x/MeV << "\t" << y << "\n";
    }

    ofFile.close();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BGCPhysicsList::SetCuts()
{
    G4ProductionCutsTable::GetProductionCutsTable()->SetEnergyRange(0.001*eV, 10*TeV);

    defaultCutValue = 0.1*nm;
    // defaultCutValue = 0.7*mm;
    SetCutValue(defaultCutValue, "e-");


    SetCutValue(defaultCutValue, "gamma");
    SetCutValue(defaultCutValue, "e+");
    SetCutValue(defaultCutValue, "proton");
}