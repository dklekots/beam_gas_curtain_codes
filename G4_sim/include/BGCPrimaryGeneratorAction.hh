#ifndef __BGCPRIMARYGENERATORACTION_HH_
#define __BGCPRIMARYGENERATORACTION_HH_

#include "G4VUserPrimaryGeneratorAction.hh"

class BGCPrimaryGeneratorAction: public G4VUserPrimaryGeneratorAction
{
public:
    BGCPrimaryGeneratorAction();
    ~BGCPrimaryGeneratorAction();


    virtual void GeneratePrimaries(G4Event*);


private:

};


#endif //__BGCPRIMARYGENERATORACTION_HH_