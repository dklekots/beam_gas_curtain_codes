#ifndef __BGCTRAKINGACTION_HH__
#define __BGCTRAKINGACTION_HH__

#include "G4UserTrackingAction.hh"

class BGCTrackingAction: public G4UserTrackingAction
{
public:
    BGCTrackingAction();
    ~BGCTrackingAction();

    void PreUserTrackingAction(const G4Track*);
    void PostUserTrackingAction(const G4Track*);
};


#endif //__BGCTRAKINGACTION_HH__