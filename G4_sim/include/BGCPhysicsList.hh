#ifndef __BGCPHYSICSLIST_HH__
#define __BGCPHYSICSLIST_HH__

#include "G4VUserPhysicsList.hh"

class BGCPhysicsList: public G4VUserPhysicsList
{
public:
    BGCPhysicsList();
    ~BGCPhysicsList();

    void ConstructParticle();
    void ConstructProcess();
    void SetCuts();
};

#endif //__BGCPHYSICSLIST_HH__