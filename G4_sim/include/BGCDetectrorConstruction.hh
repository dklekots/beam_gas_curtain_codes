#ifndef __BGCDETECTORCONSTRUCTION_HH__
#define __BGCDETECTORCONSTRUCTION_HH__


#include "G4VUserDetectorConstruction.hh"

class BGCDetectorConstruction : public G4VUserDetectorConstruction
{

public:
    BGCDetectorConstruction();
    ~BGCDetectorConstruction() override;


public:
    G4VPhysicalVolume* Construct() override;

};


#endif //__DETECTORCONSTRUCTION_HH__