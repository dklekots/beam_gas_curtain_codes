
#ifndef BGCRunAction_h
#define BGCRunAction_h 1

#include "G4UserRunAction.hh"

class G4Run;

class BGCRunAction : public G4UserRunAction
{
public:
    BGCRunAction();
    virtual ~BGCRunAction();

    virtual void BeginOfRunAction(const G4Run*);
    virtual void EndOfRunAction(const G4Run*);
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif //BGCRunAction_h

