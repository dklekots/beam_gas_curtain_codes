#ifndef __BGCACTIOPNINITIALIZATION_HH__
#define __BGCACTIOPNINITIALIZATION_HH__


#include <G4VUserActionInitialization.hh>

class BGCActionInitialization : public G4VUserActionInitialization
{
public:
    BGCActionInitialization() = default;
    ~BGCActionInitialization() override = default;

    void BuildForMaster() const override;
    void Build() const override;
};


#endif