#include "G4RunManagerFactory.hh"

#include "G4UIExecutive.hh"
#include "G4VisExecutive.hh"
#include "G4UImanager.hh"

#include "BGCPhysicsList.hh"


#include "BGCActionInitialization.hh"
#include "BGCDetectrorConstruction.hh"

#include "G4String.hh"

int main(int argc, char** argv)
{

    G4UIExecutive* ui = new G4UIExecutive(argc, argv);

    G4RunManager* runManager =
        G4RunManagerFactory::CreateRunManager(G4RunManagerType::Default);

    runManager->SetUserInitialization(new BGCDetectorConstruction());

    runManager->SetUserInitialization(new BGCPhysicsList);

    runManager->SetUserInitialization(new BGCActionInitialization);

    G4VisManager* visManager = new G4VisExecutive;
    visManager->Initialize();


    G4UImanager* UImanager = G4UImanager::GetUIpointer();

    if(argc == 2)
    {
        G4String macro = argv[1];
        G4String command = "/control/execute ";
        UImanager->ApplyCommand(command + macro);
    }
    else
    {
        UImanager->ApplyCommand("/control/execute init_vis.mac");
        ui->SessionStart();
        delete ui;
    }

    delete visManager;
    delete runManager;

    return 0;
}