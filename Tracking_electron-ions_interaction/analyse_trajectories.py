import pandas as pd
import matplotlib.pyplot as plt

nParticles = 1000


fig, (ax1, ax2) = plt.subplots(1,2, figsize=(15, 7))

trakingOutFileName = "OutFile.txt"
file = open(trakingOutFileName, "r")

step = int(0)
for stepLine in file:
    step+=1

    print("Current step is", step)

    synamicPars = stepLine.split()
    x_e = [float(synamicPars[i             ]) for i in range(0,int(nParticles/2))]
    y_e = [float(synamicPars[i+  nParticles]) for i in range(0,int(nParticles/2))]
    z_e = [float(synamicPars[i+2*nParticles]) for i in range(0,int(nParticles/2))]

    x_i = [float(synamicPars[i+  int(nParticles/2)]) for i in range(0,int(nParticles/2))]
    y_i = [float(synamicPars[i+3*int(nParticles/2)]) for i in range(0,int(nParticles/2))]
    z_i = [float(synamicPars[i+5*int(nParticles/2)]) for i in range(0,int(nParticles/2))]

    # ax1.hist2d(x,y, bins=20)
    ax1.scatter(x_e,y_e, marker=".", color="b")
    ax1.scatter(x_i,y_i, marker=".", color="r")
    ax1.set_xlabel("x [mm]")
    ax1.set_ylabel("y [mm]")
    ax1.set_xlim([-0.007, 0.007])
    ax1.set_ylim([-0.007, 0.007])
    ax1.set_title("time = {:.4f} ns".format(2e-4*step))

    # ax2.hist2d(z,y, bins=20)
    ax2.scatter(z_e,y_e, marker=".", color="b")
    ax2.scatter(z_i,y_i, marker=".", color="r")
    ax2.set_xlabel("z [mm]")
    ax2.set_ylabel("y [mm]")
    ax2.set_xlim([-0.155, 0.11])
    ax2.set_ylim([-0.007, 0.007])

    plotsDirectory = "plots"
    fig.savefig(plotsDirectory + f"/step_{step}.png")
    
    
    ax1.clear()
    ax2.clear()
    # fig.clear()
    
    # plt.close()
    # plt.cla()
    # plt.clf()

    # del ax1
    # del ax2
    # del fig
    del x_e
    del y_e
    del z_e

    del x_i
    del y_i
    del z_i
