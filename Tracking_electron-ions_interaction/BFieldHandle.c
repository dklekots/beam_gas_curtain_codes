#include "BFieldHandle.h"

#include <stdio.h>
#include <stdlib.h>

BFieldHandle* LoadFieldMap(const char* filename, double mapStep)
{
    

    FILE *file = fopen(filename,"r");

    char headerLine[128];
    fgets(headerLine, sizeof(headerLine), file);

    double minX, maxX;
    double minY, maxY;
    double minZ, maxZ;

    double x, y, z, Bx, By, Bz;

    fscanf(file, "%lf,%lf,%lf,%lf,%lf,%lf", 
            &minX,&minY,&minZ,&Bx,&By,&Bz);            

    while(fscanf(file, "%lf,%lf,%lf,%lf,%lf,%lf",&x,&y,&z,&Bx,&By,&Bz) != EOF)
    {
        if (x < minX) minX = x;
        if (x < minY) minY = y;
        if (z < minZ) minZ = z;
        if (x > maxX) maxX = x;
        if (y > maxY) maxY = y;
        if (z > maxZ) maxZ = z;
    }

    BFieldHandle* result = malloc(sizeof(BFieldHandle));

    result->mapStep = mapStep;

    result->xMinPoint = minX;
    result->yMinPoint = minY;
    result->zMinPoint = minZ;

    result->xMaxPoint = maxX;
    result->yMaxPoint = maxY;
    result->zMaxPoint = maxZ;

    if(maxX == minX || maxY == minY || maxZ == minZ)
    {
        printf("ERROR: The field map is not correct!");
        exit(1);
    }

    result->xIdxSize = (maxX-minX) / mapStep + 1;
    result->yIdxSize = (maxY-minY) / mapStep + 1;
    result->zIdxSize = (maxZ-minZ) / mapStep + 1;


    result->BxMap = malloc(result->xIdxSize*sizeof(double**));
    result->ByMap = malloc(result->xIdxSize*sizeof(double**));
    result->BzMap = malloc(result->xIdxSize*sizeof(double**));

    for(int xIdx = 0; xIdx < result->xIdxSize; ++xIdx)
    {
        result->BxMap[xIdx] = malloc(result->yIdxSize*sizeof(double*));
        result->BzMap[xIdx] = malloc(result->yIdxSize*sizeof(double*));
        result->ByMap[xIdx] = malloc(result->yIdxSize*sizeof(double*));

        for(int yIdx = 0; yIdx < result->yIdxSize; ++yIdx)
        {
            result->BxMap[xIdx][yIdx] = malloc(result->zIdxSize*sizeof(double));
            result->BzMap[xIdx][yIdx] = malloc(result->zIdxSize*sizeof(double));
            result->ByMap[xIdx][yIdx] = malloc(result->zIdxSize*sizeof(double));
        }
    }


    rewind(file);//return to the beginning of the file

    fgets(headerLine, sizeof(headerLine), file);


    while(fscanf(file, "%lf,%lf,%lf,%lf,%lf,%lf",&x,&y,&z,&Bx,&By,&Bz) != EOF)
    {
        int xIdx = (x - result->xMinPoint)/mapStep;
        int yIdx = (y - result->yMinPoint)/mapStep;
        int zIdx = (z - result->zMinPoint)/mapStep;

        result->BxMap[xIdx][yIdx][zIdx] = Bx;
        result->ByMap[xIdx][yIdx][zIdx] = By;
        result->BzMap[xIdx][yIdx][zIdx] = Bz;
    }


    fclose(file);

    return result;
}

void GetBfield(const BFieldHandle* aHandle, const double* restrict xM, const double* restrict yM, const double* restrict zM,
                 double* restrict BxOut, double * restrict ByOut, double * restrict BzOut)
{
    // scale to millimetres, as the field map is in millimetres
    double x = *xM * 1000;
    double y = *yM * 1000;
    double z = *zM * 1000;

    double*** BxM = aHandle->BxMap;
    double*** ByM = aHandle->ByMap;
    double*** BzM = aHandle->BzMap;

    double xMinPoint = aHandle->xMinPoint;
    double yMinPoint = aHandle->yMinPoint;
    double zMinPoint = aHandle->zMinPoint;    

    double ms = aHandle->mapStep;

    int xIdx = (x - xMinPoint)/ms;
    int yIdx = (y - yMinPoint)/ms;
    int zIdx = (z - zMinPoint)/ms;

    // pervent from going outside the array range 
    // in case point in on the map border
    if (x == aHandle->xMaxPoint) xIdx--;
    if (y == aHandle->yMaxPoint) yIdx--;
    if (z == aHandle->zMaxPoint) zIdx--;

    // we will work only from residual from the map in furthers
    x -= ms*xIdx + xMinPoint;
    y -= ms*yIdx + yMinPoint;
    z -= ms*zIdx + zMinPoint;

    ////////////////////
    // Bx
    ////////////////////

    double Bx[] = {BxM[xIdx+1][yIdx+1][zIdx+1],BxM[xIdx][yIdx+1][zIdx+1],
                   BxM[xIdx+1][yIdx][zIdx+1],BxM[xIdx+1][yIdx+1][zIdx],
                   BxM[xIdx+1][yIdx][zIdx],BxM[xIdx][yIdx+1][zIdx],
                   BxM[xIdx][yIdx][zIdx+1],BxM[xIdx][yIdx][zIdx]};

    double A = (Bx[0]-Bx[1]-Bx[2]-Bx[3]+Bx[4]+Bx[5]+Bx[6]-Bx[7])/(ms*ms*ms);
    double B = (Bx[1]-Bx[5]-Bx[6]+Bx[7])/(ms*ms);
    double C = (Bx[2]-Bx[4]-Bx[6]+Bx[7])/(ms*ms);
    double D = (Bx[3]-Bx[4]-Bx[5]+Bx[7])/(ms*ms);
    double E = (Bx[4]-Bx[7])/ms;
    double F = (Bx[5]-Bx[7])/ms;
    double G = (Bx[6]-Bx[7])/ms;
    double H = Bx[7];
    
    *BxOut = A*x*y*z + B*y*z + C*x*z + D*x*y + E*x + F*y + G*z + H;

    ////////////////////
    // By
    ////////////////////

    double By[] = {ByM[xIdx+1][yIdx+1][zIdx+1],ByM[xIdx][yIdx+1][zIdx+1],
                   ByM[xIdx+1][yIdx][zIdx+1],ByM[xIdx+1][yIdx+1][zIdx],
                   ByM[xIdx+1][yIdx][zIdx],ByM[xIdx][yIdx+1][zIdx],
                   ByM[xIdx][yIdx][zIdx+1],ByM[xIdx][yIdx][zIdx]};

    A = (By[0]-By[1]-By[2]-By[3]+By[4]+By[5]+By[6]-By[7])/(ms*ms*ms);
    B = (By[1]-By[5]-By[6]+By[7])/(ms*ms);
    C = (By[2]-By[4]-By[6]+By[7])/(ms*ms);
    D = (By[3]-By[4]-By[5]+By[7])/(ms*ms);
    E = (By[4]-By[7])/ms;
    F = (By[5]-By[7])/ms;
    G = (By[6]-By[7])/ms;
    H = By[7];    

    *ByOut = A*x*y*z + B*y*z + C*x*z + D*x*y + E*x + F*y + G*z + H;

    ////////////////////
    // By
    ////////////////////

    double Bz[] = {BzM[xIdx+1][yIdx+1][zIdx+1],BzM[xIdx][yIdx+1][zIdx+1],
                   BzM[xIdx+1][yIdx][zIdx+1],BzM[xIdx+1][yIdx+1][zIdx],
                   BzM[xIdx+1][yIdx][zIdx],BzM[xIdx][yIdx+1][zIdx],
                   BzM[xIdx][yIdx][zIdx+1],BzM[xIdx][yIdx][zIdx]};    

    A = (Bz[0]-Bz[1]-Bz[2]-Bz[3]+Bz[4]+Bz[5]+Bz[6]-Bz[7])/(ms*ms*ms);
    B = (Bz[1]-Bz[5]-Bz[6]+Bz[7])/(ms*ms);
    C = (Bz[2]-Bz[4]-Bz[6]+Bz[7])/(ms*ms);
    D = (Bz[3]-Bz[4]-Bz[5]+Bz[7])/(ms*ms);
    E = (Bz[4]-Bz[7])/ms;
    F = (Bz[5]-Bz[7])/ms;
    G = (Bz[6]-Bz[7])/ms;
    H = Bz[7];

    *BzOut = A*x*y*z + B*y*z + C*x*z + D*x*y + E*x + F*y + G*z + H;

}


void deleteBfieldHandle(BFieldHandle* aHandle)
{
    for(int xIdx = 0; xIdx < aHandle->xIdxSize; ++xIdx)
    {
        for(int yIdx = 0; yIdx < aHandle->yIdxSize; ++yIdx)
        {
            free(aHandle->BxMap[xIdx][yIdx]);
            free(aHandle->BzMap[xIdx][yIdx]);
            free(aHandle->ByMap[xIdx][yIdx]);
        }
        free(aHandle->BxMap[xIdx]);
        free(aHandle->BzMap[xIdx]);
        free(aHandle->ByMap[xIdx]);
    }

    free(aHandle->BxMap);
    free(aHandle->ByMap);
    free(aHandle->BzMap);  

    free(aHandle);

}