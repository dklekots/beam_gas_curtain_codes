import vtk

trakingOutFileName = "OutFile.txt";
file = open(trakingOutFileName, "r")

nParticles = 1000
# indexes is [particleNum][stepNum]
x_arr = [[] for i in range(nParticles)]
y_arr = [[] for i in range(nParticles)]
z_arr = [[] for i in range(nParticles)]

for stepLine in file:
    synamicPars = stepLine.split()

    for particleId in range(nParticles):
        x_arr[particleId].append(float(synamicPars[particleId               ]))
        y_arr[particleId].append(float(synamicPars[particleId + nParticles  ]))
        z_arr[particleId].append(float(synamicPars[particleId + 2*nParticles]))

print("The input file was scanned")

#################################################################################

def create_trajectory_actor():

    points = vtk.vtkPoints()
    lines = vtk.vtkCellArray()
    vertices = vtk.vtkCellArray()

    polyData = vtk.vtkPolyData()
    polyData.SetPoints(points)
    polyData.SetLines(lines)
    polyData.SetVerts(vertices)

    mapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        mapper.SetInput(polyData)
    else:
        mapper.SetInputData(polyData)

    actor = vtk.vtkActor()
    actor.SetMapper(mapper)

    return actor, points, lines, vertices



def create_label_actor():

    textProperty = vtk.vtkTextProperty()
    textProperty.SetJustificationToCentered()
    textProperty.SetFontSize(int(600 / 20))

    mapper = vtk.vtkTextMapper()
    # mapper.SetInput("textLabel")
    mapper.SetTextProperty(textProperty)

    actor = vtk.vtkActor2D()
    actor.SetMapper(mapper)
    actor.SetPosition(600 / 2.0, 16)
    colors = vtk.vtkNamedColors()
    actor.GetProperty().SetColor(colors.GetColor3d("Gold"))
    return actor



def MakeAxesActor():
    """
    Make an axis actor.

    :return: The axis actor.
    """
    axes = vtk.vtkAxesActor()
    axes.SetShaftTypeToCylinder()
    axes.SetXAxisLabelText('X 10 mm')
    axes.SetYAxisLabelText('Y 10 mm')
    axes.SetZAxisLabelText('Z 10 mm')
    axes.SetTotalLength(0.01, 0.01, 0.01)
    axes.SetCylinderRadius(0.5 * axes.GetCylinderRadius())
    axes.SetConeRadius(0.75 * axes.GetConeRadius())
    axes.SetSphereRadius(0.5 * axes.GetSphereRadius())
    axes.GetXAxisCaptionActor2D().GetTextActor().GetScaledTextProperty()
    axes.GetXAxisCaptionActor2D().GetTextActor().SetTextScaleModeToNone()
    axes.GetYAxisCaptionActor2D().GetTextActor().GetScaledTextProperty()
    axes.GetYAxisCaptionActor2D().GetTextActor().SetTextScaleModeToNone()
    axes.GetZAxisCaptionActor2D().GetTextActor().GetScaledTextProperty()
    axes.GetZAxisCaptionActor2D().GetTextActor().SetTextScaleModeToNone()
    return axes


renderer = vtk.vtkRenderer()
render_window = vtk.vtkRenderWindow()
render_window.AddRenderer(renderer)
render_window.SetSize(1000, 700)

interactor = vtk.vtkRenderWindowInteractor()
interactor.SetRenderWindow(render_window)

colors = vtk.vtkNamedColors()

actors = []
for i in range(nParticles):

    # electrons are colored in Blue, ions are colored in Red
    r,g,b = colors.GetColor3d("Blue") if i < nParticles/2 else colors.GetColor3d("Red")

    actor, points, lines, vertices = create_trajectory_actor()

    actor.GetProperty().SetColor(r, g, b)
    actor.GetProperty().SetPointSize(3)
    renderer.AddActor(actor)
    actors.append((actor, points, lines, vertices))

renderer.SetBackground(colors.GetColor3d("Black"))
renderer.AddActor(MakeAxesActor())

textActor = create_label_actor()
renderer.AddActor(textActor)


# textActor.GetMapper().Update()

def update_trajectory(i):

    for traj_i in range(len(actors)):

        actor, points, lines, vertices = actors[traj_i]

        points.Reset()
        lines.Reset()
        vertices.Reset()

        x = x_arr[traj_i]
        y = y_arr[traj_i]
        z = z_arr[traj_i]

        step_shift = 10
        num_points = min(len(x), i+1)
        min_num_points = max(0, num_points-step_shift)

        for j in range(min_num_points, num_points):
            points.InsertNextPoint(x[j], y[j], z[j])

        vertex = vtk.vtkVertex()
        vertex.GetPointIds().SetId(0, step_shift-1)
        vertices.InsertNextCell(vertex)

        for j in range(0, step_shift-1):
            line = vtk.vtkLine()
            line.GetPointIds().SetId(0, j)
            line.GetPointIds().SetId(1, j+1)
            lines.InsertNextCell(line)
            
        actor.GetMapper().Update()

        textActor.GetMapper().SetInput("time = {:.4f} ns".format(2e-4*(i+1)))

    render_window.Render()

interactor.Initialize()
animation_steps = len(x_arr[0])
interactor.CreateRepeatingTimer(16)

def on_timer_event(obj, event):
    on_timer_event.step = (on_timer_event.step + 1) % animation_steps
    update_trajectory(on_timer_event.step)
on_timer_event.step = 0

interactor.AddObserver("TimerEvent", on_timer_event)

interactor.Start()