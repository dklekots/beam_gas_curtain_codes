import os
import moviepy.video.io.ImageSequenceClip
plotsDirectory='plots'
fps=64

image_files = [os.path.join(plotsDirectory, f"step_{i+1}.png")
               for i in range(10000)]
clip = moviepy.video.io.ImageSequenceClip.ImageSequenceClip(image_files, fps=fps)
clip.write_videofile('video.mp4')