#ifndef __BFIELDHANDLE__H__
#define __BFIELDHANDLE__H__

struct BFieldHandleObj
{

    double mapStep;

    double xMinPoint;
    double yMinPoint;
    double zMinPoint;

    double xMaxPoint;
    double yMaxPoint;
    double zMaxPoint;

    int xIdxSize;
    int yIdxSize;
    int zIdxSize;

    //indexes [xIdx][yIdx][zIdx]
    double*** BxMap;
    double*** ByMap;
    double*** BzMap;

};

typedef struct BFieldHandleObj BFieldHandle;

/*
    The filename should be in .csv format with the columns
    x, y, z, Bx, By, Bz

    mapStep is the distance between the points in matric in millimeters

    Where x y z represent coordinates of the point in millimetres
    Bx By Bx represent the magnetic field in Tesla

    First line of the file will be ignored (it can contain headers)
*/
BFieldHandle* LoadFieldMap(const char* filename, double mapStep);

void deleteBfieldHandle(BFieldHandle*);


/*
    x, y, z shall be in meters
 */
void GetBfield(const BFieldHandle* aHandle, const double* restrict x, const double* restrict y, const double* restrict z,
                 double* restrict eBx, double * restrict eBy, double * restrict eBz);

#endif //__BFIELDHANDLE__H__
