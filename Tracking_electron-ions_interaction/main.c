// run 'gcc main.c BFieldHandle.h BFieldHandle.c -lm -fopenmp'
// to compile this code

#include <stdio.h>
#include <math.h>

#include <omp.h>

#include "BFieldHandle.h"


const int nParticles = 1000;

int pChar[1000]; 
double particleMasses[1000];

BFieldHandle* fieldHangle;


void CalculateInteractions(const int* restrict n, const double *restrict x, double *restrict f)
{

    const double K = 2.307077540832e-28; //N*m^2
    const double e = 1.60217663E-19; //coulomb

    const double *xCoor = x;
    const double *yCoor = x + nParticles;
    const double *zCoor = x + 2*nParticles;

    const double *xDot = x + 3*nParticles;
    const double *yDot = x + 4*nParticles;
    const double *zDot = x + 5*nParticles;


    double* fx = f;
    double* fy = f + nParticles;
    double* fz = f + 2*nParticles;

    double* fxDot = f + 3*nParticles;
    double* fyDot = f + 4*nParticles;
    double* fzDot = f + 5*nParticles;

    // interaction with magnetic field and fx, fy, fz
    #pragma omp parallel for default(shared)
    for(int i = 0; i < nParticles; ++i)
    {
        fx[i] = xDot[i];
        fy[i] = yDot[i];
        fz[i] = zDot[i];

        double eBx;
        double eBy;
        double eBz;
        
        GetBfield(fieldHangle, xCoor+i, yCoor+i, zCoor+i, &eBx, &eBy, &eBz);

        eBx*=e;
        eBy*=e;
        eBz*=e;

        fxDot[i] = pChar[i]*(yDot[i]*eBz - zDot[i]*eBy);
        fyDot[i] = pChar[i]*(zDot[i]*eBx - xDot[i]*eBz);
        fzDot[i] = pChar[i]*(xDot[i]*eBy - yDot[i]*eBx);
    }

    #pragma omp parallel for default(shared)
    for(int i = 0; i < nParticles; ++i)
    {
        for(int j = i+1; j < nParticles; ++j)
        {
            double Rij = (xCoor[i] - yCoor[j])*(xCoor[i] - yCoor[j]);
            Rij += (yCoor[i] - yCoor[j])*(yCoor[i] - yCoor[j]);
            Rij += (zCoor[i] - zCoor[j])*(zCoor[i] - zCoor[j]);
            Rij = sqrt(Rij);

            double Fx_ij = -pChar[i]*pChar[j]*K*(xCoor[j]-xCoor[i])/(Rij*Rij*Rij);
            double Fy_ij = -pChar[i]*pChar[j]*K*(yCoor[j]-yCoor[i])/(Rij*Rij*Rij);
            double Fz_ij = -pChar[i]*pChar[j]*K*(zCoor[j]-zCoor[i])/(Rij*Rij*Rij);

            fxDot[i] += Fx_ij;
            fyDot[i] += Fy_ij;
            fzDot[i] += Fz_ij;

            fxDot[j] -= Fx_ij;
            fyDot[j] -= Fy_ij;
            fzDot[j] -= Fz_ij;            
        }
    }

    #pragma omp parallel for default(shared)
    for(int i = 0; i < nParticles; ++i)
    {
        fxDot[i]/=particleMasses[i];
        fyDot[i]/=particleMasses[i];
        fzDot[i]/=particleMasses[i];
    }

}


void calcRungeKutta(const int* restrict nPars, double* restrict x0, const int* restrict numOfSteps,
                    const double* restrict stepTime, FILE * restrict outFile)
{

    double xNext[*nPars];
    
    double k1[*nPars];
    double k2[*nPars];
    double k3[*nPars];
    double k4[*nPars]; 

    for(int i = 0; i < *numOfSteps; ++i)
    {

        CalculateInteractions(nPars, x0, k1);

        #pragma omp parallel for shared(xNext, x0, k1, stepTime)
        for(int j=0; j < *nPars; ++j)
            xNext[j] = x0[j] + (*stepTime)*k1[j]/2.0;

        CalculateInteractions(nPars, xNext, k2);

        #pragma omp parallel for shared(xNext, x0, k2, stepTime)
        for(int j=0; j < *nPars; ++j)
            xNext[j] = x0[j] + (*stepTime)*k2[j]/2.0;

        CalculateInteractions(nPars, xNext, k3);

        #pragma omp parallel for shared(xNext, x0, k3, stepTime)
        for(int j=0; j < *nPars; ++j)
            xNext[j] = x0[j] + (*stepTime)*k3[j];

        CalculateInteractions(nPars, xNext, k4);

        #pragma omp parallel for shared(x0, k1, k2, k3, k4, stepTime)
        for(int j=0; j < *nPars; ++j)
            x0[j] += (*stepTime)*(k1[j] + 2*k2[j] + 2*k3[j] + k4[j])/6;


        for(int j=0; j < *nPars; ++j)
            fprintf(outFile, "%.15e ", x0[j]);
        
        fprintf(outFile, "\n");

        // lines of code po print current process
        static int stepNum = 0;
        stepNum++;
        if (stepNum % 100 == 0)
            printf("Current step number is %d\n", stepNum);

    }

}


int main(int *argc, char **argv)
{
    const char* BfieldMapFileName = "B-Field [Ms] sorted.csv";
    fieldHangle = LoadFieldMap(BfieldMapFileName, 10);

    ////////////////////////////////////////////////////////////
    /// Filling initial coordinates and properties of particles.
    ////////////////////////////////////////////////////////////

    // note, the first half particles in simulation is for electrons,
    // the second half particles is for ions,
    // the order is saved during the simulation

    const char* electronInitCoordinatesFileName = "geant4_out_electrons.csv";
    FILE* initFile_e = fopen(electronInitCoordinatesFileName, "r");
    const char* ionInitCoordinatesFileName = "geant4_out_ions.csv";
    FILE* initFile_i = fopen(ionInitCoordinatesFileName, "r");

    // skipping first line
    char headerLine[128];
    fgets(headerLine, sizeof(headerLine), initFile_e);
    fgets(headerLine, sizeof(headerLine), initFile_i);

    const int npars = 6*nParticles;

    double x0[npars];

    double* xArr = x0;
    double* yArr = x0 + nParticles;
    double* zArr = x0 + 2*nParticles;
    double* vxArr = x0 + 3*nParticles;
    double* vyArr = x0 + 4*nParticles;
    double* vzArr = x0 + 5*nParticles;

    for(int i = 0; i < nParticles/2; ++i)
    {
        int idle;
        double x,y,z,vx,vy,vz;

        fscanf(initFile_e, "%d,%lf,%lf,%lf,%lf,%lf,%lf", &idle,&x,&y,&z,&vx,&vy,&vz);

        xArr[i] = x;
        yArr[i] = y;
        zArr[i] = z;

        vxArr[i] = vx;
        vyArr[i] = vx;
        vzArr[i] = vz;

        pChar[i] = -1;
        particleMasses[i] = 9.1093837e-31;
    }

    for(int i = nParticles/2; i < nParticles; ++i)
    {
        int idle;
        double x,y,z,vx,vy,vz;

        fscanf(initFile_i, "%d,%lf,%lf,%lf,%lf,%lf,%lf", &idle,&x,&y,&z,&vx,&vy,&vz);

        xArr[i] = x;
        yArr[i] = y;
        zArr[i] = z;

        vxArr[i] = vx;
        vyArr[i] = vx;
        vzArr[i] = vz;

        pChar[i] = 1;
        particleMasses[i] = 3.350967843e-26;
    }    

    fclose(initFile_e);
    fclose(initFile_i);

    /////////////////////////////////////
    //  Solving differential equations
    /////////////////////////////////////

    const char* trakingOutFileName = "OutFile.txt";
    FILE *outFile = fopen(trakingOutFileName, "w");

    int numOfSteps = 10000;
    double stepTime = 2e-13;

    calcRungeKutta(&npars, x0, &numOfSteps, &stepTime, outFile);

    fclose(outFile);

    return 0;
}