import uproot
import numpy as np

def momentum2velocity(momentum, mass):
    '''
        momentum and mass should be in the same energy units
    '''

    return 299792458*np.sqrt(momentum**2/(momentum**2+mass*2)) # m/s


G4outRootFileName = "SecondariesG4sim.root"
flat_tree = uproot.open(G4outRootFileName)['Secondaries']
df = flat_tree.arrays(library="np")

electronInitCoordinatesFileName = "geant4_out_electrons.csv";
ionInitCoordinatesFileName = "geant4_out_electrons.csv";
file_e = open(electronInitCoordinatesFileName, "w")
file_i = open(ionInitCoordinatesFileName, "w")

file_e.write("simulation step,x,y,z,vx,vy,vz\n") # header line
file_i.write("simulation step,x,y,z,vx,vy,vz\n") # header line

for i in range(min(len(df["x"]), 1001)):
    
    # electrons

    v = momentum2velocity(df['P'][i], 511)

    vx = v * df['Px'][i]/df['P'][i]
    vy = v * df['Py'][i]/df['P'][i]
    vz = v * df['Pz'][i]/df['P'][i]

    file_e.write("1,") #step
    file_e.write(f"{df['x'][i]/1000 + vx/v * 1.4419E-7},") # divide by 1000 to convert millimeters into meters
    file_e.write(f"{df['y'][i]/1000 + vy/v * 1.4419E-7},") # vy/v * 1.4419E-7 to shift from ion location
    file_e.write(f"{df['z'][i]/1000 + vz/v * 1.4419E-7},")

    file_e.write(f"{vx},")
    file_e.write(f"{vy},")
    file_e.write(f"{vz}\n")

    # protons

    file_i.write("1,") #step
    file_i.write(f"{df['x'][i]/1000},")
    file_i.write(f"{df['y'][i]/1000},")
    file_i.write(f"{df['z'][i]/1000},")

    vx = -800 # m/s
    vy = 0
    vz = 0

    file_i.write(f"{vx},")
    file_i.write(f"{vy},")
    file_i.write(f"{vz}\n")

file_e.close()
file_i.close()